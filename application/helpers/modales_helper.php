<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( !function_exists( 'tiene_modal' ) )  {
    function tiene_modal( $marca )
    {
        $resultado = false;

        if( $marca == "PROCTER & GAMBLE FARMACIA" ) {
            $resultado = '<a href="#" id="pyg">' . $marca . '</a>';
        } else {
            $resultado = $marca;
        }

        return $resultado;
    }
}