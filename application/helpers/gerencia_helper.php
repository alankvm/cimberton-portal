<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if (!function_exists('calcular_colores'))
{
    function calcular_colores( $monto = 0)
    {
        $color = "";
        if ( $monto * 100 <= 95.00 ) {
            $color = 'alert-danger';
        } else {
            if ( $monto * 100 <= 99.00 ) {
                $color = 'alert';
            } else  {
                $color = 'alert-success';
            }
        }
        print 'class="' . $color . '"';
    }
}

if (!function_exists('resaltar_titulo'))
{
    function resaltar_titulo( $fila = "")
    {
        if ( $fila != "" ) {
            
        }
    }
}

if ( !function_exists( 'color' ) )
{
    function color( $monto = 0 )
    {
        $color = null;
        if ( $monto <= 95.00 ) {
            $color = 'alert-danger';
        } else {
            if ( $monto <= 99.00 ) {
                $color = 'alert';
            } else  {
                $color = 'alert-success';
            }
        }
        print 'class="' . $color . '"';
    }
}
