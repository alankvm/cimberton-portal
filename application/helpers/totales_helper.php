<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists( 'recalcularTotales' ) ) {
    function recalcularTotales( $total, $otros )
    {
        foreach ($total as $key => $value) {
            $total->{$key} += $otros->{$key};
        }

        return $total;
    }
}