<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( !function_exists( 'clearName' ) )  {

    function clearName( $name )
    {
        $buscar = array( '(FARMACIA)', 'DIVISION' );
        $reemplazar = array( '', 'DIV.' );
        return str_replace( $buscar, $reemplazar , $name );
    }
}