<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>C. Imberton SA de CV | <?php print $titulo ?></title>
    <meta name="author" content="Grupo Satelite" />
    <meta name="description" content="Departamento de Ventas" />
    <meta name="keywords" content="" />
    <meta name="application-name" content="Departamento De Ventas" />

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Le styles -->
    <!-- Use new way for google web fonts 
    http://www.smashingmagazine.com/2012/07/11/avoiding-faux-weights-styles-google-web-fonts -->
    <!-- Headings -->
    <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />  -->
    <!-- Text -->
    <!-- <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' /> --> 
    <!--[if lt IE 9]>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:700" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Droid+Sans:700" rel="stylesheet" type="text/css" />
    <![endif]-->

    <!-- Core stylesheets do not remove -->
    <link href="<?=base_url('css/bootstrap/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('css/bootstrap/bootstrap-responsive.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('css/supr-theme/jquery.ui.supr.css')?>" rel="stylesheet" type="text/css"/>
    <link href="<?=base_url('css/icons.css')?>" rel="stylesheet" type="text/css" />

    <!-- Plugins stylesheets -->
    <link href="<?=base_url('plugins/misc/qtip/jquery.qtip.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('plugins/misc/fullcalendar/fullcalendar.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('plugins/misc/search/tipuesearch.css')?>" type="text/css" rel="stylesheet" />

    <link href="<?=base_url('plugins/forms/uniform/uniform.default.css')?>" type="text/css" rel="stylesheet" />

    <!-- Main stylesheets -->
    <link href="<?=base_url('css/main.css')?>" rel="stylesheet" type="text/css" /> 

    <!-- Custom stylesheets ( Put your own changes here ) -->
    <link href="<?=base_url('css/custom.css')?>" rel="stylesheet" type="text/css" /> 
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="icon" href="<?=base_url('images/favicon.ico')?>" type="image/x-icon" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=base_url('images/apple-touch-icon-144-precomposed.png')?>" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=base_url('images/apple-touch-icon-114-precomposed.png')?>" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=base_url('images/apple-touch-icon-72-precomposed.png')?>" />
    <link rel="apple-touch-icon-precomposed" href="<?=base_url('images/apple-touch-icon-57-precomposed.png')?>" />
    
    <script type="text/javascript">
        //adding load class to body and hide page
        document.documentElement.className += 'loadstate';
    </script>

    </head>
    <body class="errorPage">

    <div class="container-fluid">

        <div class="errorContainer">
            <div class="page-header">
                <h1 class="center">404 <small>error</small></h1>
            </div>

            <h2 class="center">The page cannot be found.</h2>

            <div class="center">
                <a href="javascript: history.go(-1)" class="btn" style="margin-right:10px;"><span class="icon16 icomoon-icon-arrow-left-10"></span>Go back</a>
                <a href="dashboard.html" class="btn"><span class="icon16 icomoon-icon-screen"></span>Dashboard</a>
            </div>

        </div>

    </div><!-- End .container-fluid -->

    <!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>  

     <script type="text/javascript">
        // document ready function
        $(document).ready(function() {
            //------------- Some fancy stuff in error pages -------------//
            $('.errorContainer').hide();
            $('.errorContainer').fadeIn(1000).animate({
                'top': "50%", 'margin-top': +($('.errorContainer').height()/-2-30)
                }, {duration: 750, queue: false}, function() {
                // Animation complete.
            });
        });
    </script>
 
    </body>
</html>