<!DOCTYPE html>
<html lang="en">
<head>
<title>Acceso Denegado</title>
<!-- Le styles -->
<link href="<?=base_url('css/bootstrap/bootstrap.css')?>" rel="stylesheet" />
<link href="<?=base_url('css/bootstrap/bootstrap-responsive.css')?>" rel="stylesheet" />
<link href="<?=base_url('css/icons.css')?>" rel="stylesheet" type="text/css" />
<!-- Main stylesheets -->
<link href="<?=base_url('css/main.css')?>" rel="stylesheet" type="text/css" /> 

</head>
<body class="errorPage">
 	<div class="container-fluid">

        <div class="errorContainer">
            <div class="page-header">
                <h3 class="center"><?php echo $heading; ?></h3>
            </div>

            <h4 class="center"><?php echo $message; ?></h4>

            <div class="center">
                <a href="javascript: history.go(-1)" class="btn" style="margin-right:10px;"><span class="icon16 icomoon-icon-arrow-left-10"></span>Regresar</a>
                <a href="<?=base_url('panel/control')?>" class="btn"><span class="icon16 icomoon-icon-screen"></span>Inicio</a>
            </div>

        </div>

    </div><!-- End .container-fluid -->
	
	<!-- Le javascript
    ================================================== -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="<?=base_url('js/bootstrap/bootstrap.js')?>"></script>  
    <script type="text/javascript">
        // document ready function
        $(document).ready(function() {
            //------------- Some fancy stuff in error pages -------------//
            $('.errorContainer').hide();
            $('.errorContainer').fadeIn(1000).animate({
                'top': "50%", 'margin-top': +($('.errorContainer').height()/-2-30)
                }, {duration: 750, queue: false}, function() {
                // Animation complete.
            });
        });
    </script>
</body>
</html>
