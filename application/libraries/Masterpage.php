<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * Automatizacion de carga de pagina utilizando la libreria template
 *
 * @package CIMBERTON
 * @subpackage  Administracion
 * @category Utilidad
 * @author  Alan Alvarenga | alan.alvarenga@gruposatelite.net
 * @link    http://www.gruposatelite.net
 */

class Masterpage {
    var $CI;

    public function __construct ()
    {
        //Inicializando instancia de CodeIgniter
        $this->CI =& get_instance();
        $this->CI->load->helper( 'url' );
        $this->CI->load->library( 'Template' );
        $this->CI->load->model( 'model_navegacion' );
    }

    /**
     * Carga plantilla con la data seleccionada
     * @param  string $view      ubicacion fisica de la vista personalizada
     * @param  array  $labels    listado de labels a cambiar en la plantilla
     * @param  array  $data      informacion personalizada a cambiar
     * @param  array  $elementos elementos que se utilizaron en la vista personalizada (form, calendar, etc)
     * @param  array  $js        scripts personalizados
     */
    public function cargar( $view = 'template.php', $labels = array(), $data = array(), $elementos = array(), $js = array() )
    {
        $scripts = null;
        // Escribir labels
        if ( count($labels) > 0 ) {
            foreach ( $labels as $label ) {
                //Verificar si las regiones a las que pertenecen los label existen de lo contrario no hacer nada
                if ( isset( $this->CI->template->regions[ array_keys( $label )[0] ] ) ) {
                    //Escribir region
                    $this->CI->template->write( array_keys( $label )[0], array_values( $label )[0] );    
                }
            }    
        }

        //Cargar JS requeridos para la correcta funcionalidad de la pagina 
        $scripts_requeridos = $this->cargarJavaScript( $elementos );
        //Si se han enviado JS personalizados, concatenar a los js requeridos
        if ( count($js) > 0 ) {
            $scripts = array_merge(  $scripts_requeridos, $js );    
        }
        
        //Enviar los JS a la vista
        $data['scripts'] = ( count( $scripts ) > 0 ) ? $scripts : null;
        //Cargar listado de opciones de Navegacion
        $data['opciones'] = $this->CI->model_navegacion->getMenuNavegacion();
        // Escribir vista en el template
        $this->CI->template->write_view('content', $view, $data, TRUE);
        $this->CI->template->render();
    }

    /**
     * Cargar listado de scripts necesarios para el correcto funcionamiento de la pagina, 
     * dependiendo de los elementos cargados en la pagina
     * @param  array  $elementos listado de elementos utilizados
     * @return array listado de js necesarios
     */
    public function cargarJavaScript( $elementos = array() )
    {
        //Inicializando variables
        $scripts_resultado = array();
        // Si se han enviado elementos (form, calendar, wizard, etc)
        if ( count( $elementos) > 0 ) {
            //Recorrer elementos
            foreach ($elementos as $elemento) {
                //Verificar que elementos se han utilizado
                switch ( $elemento ) {
                    case 'form':
                        $form_scripts = array(
                            'plugins/forms/watermark/jquery.watermark.min.js',
                            'plugins/forms/elastic/jquery.elastic.js',
                            'plugins/forms/ibutton/jquery.ibutton.min.js',
                            'plugins/forms/uniform/jquery.uniform.min.js',
                            'plugins/forms/inputlimiter/jquery.inputlimiter.1.3.min.js',
                            'plugins/forms/validate/jquery.validate.min.js',
                            'plugins/forms/maskedinput/jquery.maskedinput-1.3.min.js',
                            'plugins/forms/select/select2.min.js',
                            'plugins/forms/smartWizzard/jquery.smartWizard-2.0.min.js',
                            'assets/grocery_crud/js/jquery_plugins/ui/i18n/datepicker/jquery.ui.datepicker-es.js'
                            );
                        
                        $scripts_resultado = array_merge( $scripts_resultado, $form_scripts );
                        break;
                    case 'widget':
                        $widget_scripts = array(
                            'plugins/misc/nicescroll/jquery.nicescroll.min.js',
                            'plugins/misc/jqueryFileTree/jqueryFileTree.js',
                            );
                        $scripts_resultado = array_merge( $scripts_resultado, $widget_scripts );
                        break;
                    case 'table':
                        $table_scripts = array(
                            'plugins/tables/responsive-tables/responsive-tables.js'
                            );
                        $scripts_resultado = array_merge( $scripts_resultado, $table_scripts );
                        break;
                    
                    default:
                        # code... Agrega mas casos y js
                        break;
                }
            }
        }
        
        //Enviar JS requeridos
        return $scripts_resultado;
    }

    /**
     * Carga template especializada para la generacion de CRUD con la libreria grocery CRUD
     * @param  object $output crud generado por grocery_CRUD
     * @return object vista renderizada
     */
    public function cargarCrud( $output = null )
    {
        
        //Cargar listado de opciones de Navegacion
        $data['opciones'] = $this->CI->model_navegacion->getMenuNavegacion();
        $data['output'] = $output;
        $this->CI->load->view('crud_generico.php', $output);
    }


}

/* End of file Masterpage.php */