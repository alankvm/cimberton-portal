<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * Configuracion para generacion de reportes
 *
 * @package CIMBERTON
 * @subpackage  Tablero
 * @category Gerencia
 * @author  Alan Alvarenga | alan.alvarenga@gruposatelite.net
 * @link    http://www.gruposatelite.net
 */

class Configuracion {
    private $CI;
    public $mes;
    public $year;
    public $acumulado;

    public function __construct ()
    {
        //Inicializando instancia de CodeIgniter
        $this->CI =& get_instance();
    }

    /**
     * Adquire Fecha actual y todos sus valores relacionados
     * @return object fecha actual
     */
    public function getFechaActual()
    {
        return $fecha_actual = new DateTime;
    }

    /**
     * Definir periodo activo para generacion de reportes de usuario
     * @param integer $mes          Mes seleccionado
     * @param integer $year         Año seleccionado
     * @param boolean $es_acumulado es reporte acumulado?
     */
    public function setPeriodoActivo( $mes = 0, $year = 0, $es_acumulado = false )
    {
        $this->CI->session->set_userdata( 'mes', $mes );
        $this->CI->session->set_userdata( 'year', $year );
        $this->CI->session->set_userdata( 'acumulado', ( $es_acumulado != false) ? true : false );
        
        return true;
    }

    /**
     * Establece configuracion inicial para la carga de reportes
     */
    public function setConfiguracion()
    {
        //Obtener fecha actual
        $fecha = $this->getFechaActual();

        //Verificar esta de los datos de session
        if ( $this->CI->session->userdata('mes') == false 
            && $this->CI->session->userdata('year')  == false ) {
            $this->mes  = $fecha->format('m');
            $this->year = $fecha->format('Y');
        } else if ( $this->CI->session->userdata('mes') == false 
            && $this->CI->session->userdata('year')  != false 
            && $this->CI->session->userdata('acumulado') != false ) {
            $this->mes       = false;
            $this->year      = $this->CI->session->userdata('year');
            $this->acumulado = $this->CI->session->userdata('acumulado');
        } else {
            $this->mes  = $this->CI->session->userdata('mes');
            $this->year = $this->CI->session->userdata('year');
        }

        return true;
    }

    /**
     * Verifica si es periodo regular
     * @return boolean resultado de la operacion
     */
    public function esPeriodo()
    {
        $resultado = false;
        if ( isset( $this->mes ) && $this->mes != 0 && isset( $this->year ) && $this->year != 0 ) {
            $resultado = true;
        }

        return $resultado;
    }

    /**
     * Verifica si es periodo acumulado 
     * @return boolean resultado de la operacion
     */
    public function esPeriodoAcumulado()
    {
        $resultado = false;
        if ( isset( $this->year ) && $this->year != 0 && 
            isset( $this->acumulado ) && $this->acumulado != 0 ) {
            $resultado = true;
        }

        return $resultado;
    }
}