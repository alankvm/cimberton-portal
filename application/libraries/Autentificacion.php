<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * Autentificacion de usuarios
 *
 * @package CIMBERTON
 * @subpackage  Administracion
 * @category Seguridad
 * @author  Alan Alvarenga | alan.alvarenga@gruposatelite.net
 * @link    http://www.gruposatelite.net
 */

class Autentificacion {
    var $CI;
    var $sybase_connect;
    var $sybase_query;

    public function __construct ()
    {
        //Inicializando instancia de CodeIgniter
        $this->CI =& get_instance();
        $this->CI->load->helper( 'url' );
    }

    /**
     * Verificar la valides del usuario en la base de datos
     * @return [type] [description]
     */
    public function verificarUsuario( $usuario = "", $password = "")
    {
        //Generar MD5 del password introducido
        $md5_password = md5( $password );

        //Obtener resultado de la consulta
        $resultado = $this->CI->db
        ->where( 'USU_USUARIO', $usuario  )
        ->where( 'USU_PASSWORD', $md5_password  )
        ->limit( 1 )
        ->get( 'cimberton_usu_usuario');

        //Verificar existencia del usuario en el sistema SPC
        $sybase_user = $this->comprobarUsuarioSybase( $usuario );

        //Si se encontro el usuario en la base de datos
        if ( $resultado->num_rows() > 0 && $sybase_user == 1 ) {
            //Si existen resultados, actualizar hora de ingreso
            $this->actualizarHoraIngreso( $resultado->row()->USU_ID );
            //Generar datos de sesion para el usuario
            $this->generarDatosSesion( $resultado->row() );
        } 

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 && $sybase_user == 1) ? true : false;
    }

    /**
     * Actualiza hora de ingreso al sistema
     * @param  integer $id_usuario ID de usuario
     * @return boolean resultado de la operacion
     */
    public function actualizarHoraIngreso( $id_usuario = 0 )
    {
        $current = date( "Y-m-d H:i:s" );
        $this->CI->db->where( 'USU_ID', $id_usuario );
        $this->CI->db->update( 'cimberton_usu_usuario', array( 'USU_FECHA_INGRESO' => $current ) );
        return true;
    }

    /**
     * Genera datos de sesion personalizados para el usuario y los almacena en la base de datos
     * @return [type] [description]
     */
    public function generarDatosSesion( $datos_usuario )
    {
        $datos_sesion = array(
            'usuario' =>  $datos_usuario->USU_NOMBRE,
            'logged_in' => TRUE,
            'cargo' => $datos_usuario->USU_ID_CAR,
            'codigo' => $datos_usuario->USU_ID
             );

        $this->CI->session->set_userdata( $datos_sesion );
    }

    /**
     * Verificar la existencia del usuario introducido en el servidor Sybase del SPC
     * @param  string $usuario Nombre del usuario
     * @return boolean resultado de la operacion
     */
    public function comprobarUsuarioSybase( $usuario = '' )
    {
        //0-16 Notificaciones, 16-100 Elimina todo mensaje
        //Era necesario para eliminar el warning set default context to...
        sybase_min_client_severity(100);
        sybase_min_server_severity(100);
        //Conectando con Sybase
        $this->sybase_connect = sybase_connect( 'PROLIANTML350H', 'chulon', 'chulon' ) or die();

        //Verificando si el usuario existe en el sistema SPC
        $query_sybase = "SELECT * FROM USUARIOS  WHERE NOMBRE_CORTO_ID = '$usuario'";
        
        $this->sybase_query = sybase_query( $query_sybase, $this->sybase_connect );
        $fila = sybase_fetch_array( $this->sybase_query );
        
        $resultado = FALSE;

        if( is_array( $fila ) ) {
            //Verificando los resultados de la consulta
            if ( sybase_num_rows( $this->sybase_query ) > 0 ) {
                $resultado = TRUE;
            }
        }

        //Cerrando conexion
        sybase_close( $this->sybase_connect );
        return $resultado;
    }

    /**
     * Verifica si el usuario esta loggeado
     * @return boolean resultado de la operacion
     */
    public function isLoggedIn()
    {
        $resultado = false;
        $acceso_view = $this->verificarAccesoView();
        //Verificar el estado de la session
        if ( $this->CI->session->userdata('logged_in') == TRUE && $acceso_view == TRUE ) {

            $resultado = true;
        } else if ( $this->CI->session->userdata('logged_in') == TRUE ) {
            show_error( 'Advertencia: Usted no tiene acceso a esta p&aacute;gina');
        }
        
        return $resultado;
    }

    /**
     * Destroye los datos de sesion actuales
     * @return boolean resultado de la operacion
     */
    public function logOut()
    {
        return $this->CI->session->sess_destroy();
    }

    /**
     * Valida si el usuario loggeado tiene permiso de accesar a la vista seleccionada
     * @return [type] [description]
     */
    public function verificarAccesoView()
    {
        //Inicializacion de variables 
        $current = current_url(); //URL Actual
        $base = base_url(); //URL base
        $longitud_base = strlen( $base );
        $segment_url = substr( $current, $longitud_base );
        $acceso = false;
        $existe_en_menu = $this->CI->db->get_where('cimberton_opc_opcion', array( 'OPC_URL' => $segment_url ) );
        if ( is_object( $existe_en_menu ) && $existe_en_menu->num_rows() > 0 ) {
            //Buscar el segmento de url en la base
            $query_verificacion_acceso = "SELECT opcion.OPC_URL as url 
            FROM cimberton_opc_opcion AS opcion
            INNER JOIN cimberton_oxc_opcionxcargo AS oxc ON oxc.OXC_ID_OPC = opcion.OPC_ID AND oxc.OXC_ID_CAR = ?
            WHERE opcion.OPC_URL = ? ";
            //Obtener el id de cargo almacenado en la session
            $id_cargo = $this->CI->session->userdata( 'cargo' );
            //Obtener el resultado del query
            $resultado = $this->CI->db->query( $query_verificacion_acceso, array( $id_cargo, $segment_url ) ); 
            $acceso = ( ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? true : false );
        } else {
            $acceso = true;
        }

        return $acceso;
    }

    /**
     * Obtiene informacion del usuario seleccionado
     * @param  integer $id_usuario ID del usuario seleccionado [opcional]
     * @return object informacion completa del usuario registrado
     */
    public function getInformacionUsuarioActivo( $id_usuario = null )
    {
        //id_usuario es un parametro opcional, de no ser especificado obtiene el del usuario actualmente identificado
        $id_usuario = ( ( $id_usuario == null ) ? $this->CI->session->userdata('codigo') : $id_usuario );
        //Obtener resultado de la consulta
        $resultado = $this->CI->db
        ->where( 'USU_ID', $id_usuario  )
        ->limit( 1 )
        ->get( 'cimberton_usu_usuario');
        
        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado->row() : false;
    }

    /**
     * Verificar si es primer inicio de sesion
     * @param  string $password nombre usuario ingresado por el usuario
     * @return boolean resultado de la operacion
     */
    public function esPrimerLogin( $usuario = "" )
    {
        //Inicializar variables
        $informacion = $this->getInformacionUsuarioActivo();
        $md5_password = md5( $usuario );
        $primer_inicio = FALSE;

        //Verificar el password
        if ( $md5_password == $informacion->USU_PASSWORD ) {
            $primer_inicio = TRUE;
        }

        return $primer_inicio;
    }
}