<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_administracion extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        
    }  

    /**
     * Obtiene listado de cargos registrados en el sistema
     * @return object listado de cargos 
     */
    public function getListadoCargos()
    {
        $resultado = $this->db->get('cimberton_car_cargo');
        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false;
    }

    /**
     * Obtiene listados Completo de los Menus de Navegacion Disponibles
     * @return string estructura HTML - ACCORDION (jQuery) con el listado completo de menus de navegacion
     */
    public function getListadoMenusNavegacion( $id_cargo = 0 )
    {
        $accordion = '<div class="accordion" id="accordion2">';

        $menus_principales = $this->getListadoMenusPrincipales();
        //Inicializando Identificar del accordion
        $identificador = 1;
        foreach ( $menus_principales->result() as $menu_principal ) {
            //verificando estado
            $html_checked = "";
            $is_checked = $this->verificarEstadoMenu( $menu_principal->id, $id_cargo );
            if ( $is_checked ) {
                $html_checked = 'checked="checked"';
            }
            //Construyendo Cabecera del Accordion
            $accordion .= '<div class="accordion-group">';
                $accordion .= '<div class="accordion-heading">';
                    $accordion .= '<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse' . $identificador . '">';
                    $accordion .=  $menu_principal->nombre . '</a>';
                $accordion .= '</div>';
                //Construyendo Contenido del Accordion
                $accordion .= '<div id="collapse' . $identificador . '" class="accordion-body collapse" style="height: 0px; ">';
                    $accordion .= '<div class="accordion-inner">';
                        //============================================= Contenido del Accordion ========================
                            //Accion Conceder acceso al menu
                            $accordion .= '<label class="form-label span4" for="checkboxes">&iquest;Conceder Acceso?</label>';
                            $accordion .= '<input type="checkbox" name="menus[]" value="' . $menu_principal->id . '" class="span2" '. $html_checked .' />';
                            $accordion .= $this->getSubMenusNavegacion( $identificador, $menu_principal->id, $id_cargo );

                        //==============================================================================================
                    $accordion .= '</div>';
                $accordion .= '</div>';
            $accordion .= '</div>';
            //Incrementar Identificado de Acordion
            $identificador++;
        }

        $accordion .= '</div>';

        return $accordion;
    }

    /**
     * Genera estructura HTML para asignacion de permisos de navegacion
     * @param  integer $id_menu_principal ID de menu principal seleccionado
     * @param  integer $id_cargo ID cargo seleccionado
     * @return string estructura HTML accordion para asignacion de permisos de navegacion
     */
    public function getSubMenusNavegacion( $identificador, $id_menu_principal, $id_cargo )
    {
        //Inicializando Identificar del accordion
        $identificador_sub = 1;
        $accordion = '';
        $hash = 'sub_';

        //Obtenido listado de sub menus del menu seleccionado 
        $sub_menus = $this->getListadoMenusPrimerNivel( $id_menu_principal );
        
        if ( is_object( $sub_menus ) ) {
            //Inicializando Accordion
            $accordion .= '<div class="accordion" id="' . $identificador_sub . '">';

            foreach ( $sub_menus->result() as $sub_menu ) {
                $html_checked = "";
                $is_checked = $this->verificarEstadoOpcion( $sub_menu->id, $id_cargo );
                if ( $is_checked ) {
                    $html_checked = 'checked="checked"';
                }
                //Construyendo Cabecera del Accordion
                $accordion .= '<div class="accordion-group">';
                    $accordion .= '<div class="accordion-heading">';
                        $accordion .= '<a class="accordion-toggle" data-toggle="collapse" data-parent="#' . $identificador . $identificador_sub . '" href="#collapse' . $identificador . $hash . $identificador_sub . '">';
                        $accordion .=  $sub_menu->nombre . '</a>';
                    $accordion .= '</div>';
                    //Construyendo Contenido del Accordion
                    $accordion .= '<div id="collapse' . $identificador . $hash . $identificador_sub . '" class="accordion-body collapse" style="height: 0px; ">';
                        $accordion .= '<div class="accordion-inner">';
                            //============================ Contenido del Accordion ========================================
                                //Accion Conceder acceso al menu
                                $accordion .= '<label class="form-label span4" for="checkboxes">&iquest;Conceder Acceso?</label>';
                                $accordion .= '<input type="checkbox" name="sub_menus[]" value="' . $sub_menu->id . '" class="span2" '. $html_checked .' />';
                                //Listado de opciones de menu
                                $accordion .= $this->getOpcionesMenu( $sub_menu->id, $id_cargo );
                            //==============================================================================================
                        $accordion .= '</div>';
                    $accordion .= '</div>';
                $accordion .= '</div>';
                //Incrementar Identificado de Acordion
                $identificador_sub++;
            }

            $accordion .= '</div>';
        }
        
        return $accordion;
    }

    /**
     * Obtiene listado de opciones de menu
     * @param  integer $id_opcion_padre ID de Sub Menu padre
     * @return string listado de opciones de menu hijas 
     */
    public function getOpcionesMenu( $id_opcion_padre, $id_cargo = 1 )
    {
        $opciones = $this->getListadoMenusSegundoNivel( $id_opcion_padre );

        $tabla = '<div class="page-header"> <h4>Opciones Disponibles</h4> </div>';
        $tabla .= '<table class="responsive table table-bordered">';
        $tabla .= '<thead>';
        $tabla .= '<tr>';
        $tabla .= '<th>#</th>';
        $tabla .= '<th>Opci&oacute;n</th>';
        $tabla .= '<th>&iquest;Conceder Acceso?</th>';
        $tabla .= '</thead>';
        $tabla .= '<tbody>';
         
        if ( is_object( $opciones ) && $opciones->num_rows() > 0 ) {
            
            $numero_fila = 1;

            foreach ( $opciones->result() as $opcion ) {
                $html_checked = "";
                $is_checked = $this->verificarEstadoOpcion( $opcion->id, $id_cargo );
                if ( $is_checked ) {
                    $html_checked = 'data-check="checked"';
                }

                $tabla .= '<tr>';
                $tabla .= '<td>' . $numero_fila++ . '</td>';
                $tabla .= '<td>' . $opcion->nombre . '</td>';
                $tabla .= '<td> <input type="checkbox" name="menu_opciones[]" value="' . $opcion->id . '"' . $html_checked . ' /> </td>';
                $tabla .= '</tr>';
            }
            
          
        }
        $tabla .= '</tbody>';
        $tabla .= '</table>';

        return $tabla;
    }

    /**
     * Obtiene listado de menus principales
     * @return object listado de menus principales
     */
    public function getListadoMenusPrincipales()
    {
        $query_menus = "SELECT MEN_ID AS id,
        MEN_NOMBRE AS nombre,
        MEN_URL AS url,
        MEN_DESCRIPCION AS tooltip,
        MEN_ES_PADRE AS es_padre,
        MEN_ICONO AS icono
        FROM cimberton_men_menu as menu
        GROUP BY MEN_ID ASC";

        $resultado = $this->db->query( $query_menus );

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false;
    }

    /**
     * Obtiene listado de Menus de Nivel 1 
     * @param  integer ID de menu padre
     * @return object listado de menus hijos
     */
    public function getListadoMenusPrimerNivel( $id_menu_principal )
    {
        $query_primer_nivel = "SELECT opcion.OPC_ID AS id, 
        opcion.OPC_NOMBRE AS nombre 
        FROM cimberton_opc_opcion AS opcion
        WHERE opcion.OPC_ID_MEN = ? AND opcion.OPC_NIVEL = 1";

        $resultado = $this->db->query( $query_primer_nivel, $id_menu_principal );

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false;
    }

    /**
     * Obtiene listados de menu de nivel 2
     * @param  integer $id_opcion_padre ID de opcion padre
     * @return object listado de opciones hijas
     */
    public function getListadoMenusSegundoNivel( $id_opcion_padre )
    {
        $query_segundo_nivel = "SELECT opcion.OPC_ID AS id, 
        opcion.OPC_NOMBRE AS nombre 
        FROM cimberton_opc_opcion AS opcion
        WHERE opcion.OPC_PADRE = ?  AND opcion.OPC_NIVEL = 2";

        $resultado = $this->db->query( $query_segundo_nivel, $id_opcion_padre );

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false;
    }

    /**
     * Verifica si la opcion seleccionada ha sido previamente asignada a ese cargo
     * @param  integer $id_opcion ID de opcion de menu seleccionada
     * @param  integer $id_cargo  ID de cargo seleccionado
     * @return boolean resultado la operacion
     */
    public function verificarEstadoOpcion( $id_opcion, $id_cargo )
    {
        $filtros = array( 
            'OXC_ID_OPC' => $id_opcion,
            'OXC_ID_CAR' => $id_cargo
             );
        $resultado = $this->db->get_where( 'cimberton_oxc_opcionxcargo', $filtros );

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? true : false;
    }

    /**
     * Verifica si el menu seleccionado ha sido previamente asignado a ese cargo
     * @param  integer $id_menu  ID de menu principal seleccionado
     * @param  integer $id_cargo ID de cargo seleccionado
     * @return boolean resultado de la operacion
     */
    public function verificarEstadoMenu( $id_menu, $id_cargo )
    {
        $filtros = array( 
            'MXC_ID_MEN' => $id_menu,
            'MXC_ID_CAR' => $id_cargo
             );
        $resultado = $this->db->get_where( 'cimberton_mxc_menuxcargo', $filtros );

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? true : false;
    }

    /**
     * Maneja la logica de la asignacion  de permisos de navegacion por cargo seleccionado
     * @param array $data_navegacion Listado de ID's de menu, opciones y cargos
     */
    public function setPermisosNavegacionCargo( $data_navegacion )
    {
        //Inicializar variables
        $resultado = false;
        $id_cargo  = $data_navegacion['id_cargo'];
        $sub_menus = ( isset( $data_navegacion['sub_menus'] ) ? $data_navegacion['sub_menus'] : false );
        $opciones  = ( isset( $data_navegacion['menu_opciones'] ) ? $data_navegacion['menu_opciones'] : false );
        $menus     = ( isset( $data_navegacion['menus'] ) ? $data_navegacion['menus'] : false );
        //verificar si los array enviados poseen informacion
        if ( count( $menus ) > 0 ) {
            $this->barridoPermisosNavegacion( $id_cargo, "M" );
            $this->asignarPermisosNavegacion( $menus, $id_cargo, "M" );
        }
        if ( count( $sub_menus ) > 0 && count( $opciones ) > 0 ) {
            //Realizar barrido de limpieza para el menu de primer nivel
            $this->barridoPermisosNavegacion( $id_cargo, "S" );
            //Asginar nuevos permisos de navegacion
            $resultado = $this->asignarPermisosNavegacion( $sub_menus, $id_cargo, "S");
            $resultado = $this->asignarPermisosNavegacion( $opciones, $id_cargo, "O");
        }
       
        return $resultado;
    }

    /**
     * Barrio de permisos de navegacion por cargo seleccionado
     * @param  integer $id_cargo ID cargo seleccionado
     * @param  string  $tipo     O = Opcion, M = Menu, S = Submenu
     * @return boolean tue
     */
    public function barridoPermisosNavegacion( $id_cargo = 0, $tipo = "O" )
    {
        //Verificar tipo de operacion y realizar barrido de informacion
        if ( $tipo == "O" || $tipo == "S") {
            $this->db->where( 'OXC_ID_CAR', $id_cargo );
            $this->db->delete( 'cimberton_oxc_opcionxcargo' );
        } else if ( $tipo == "M") {
            $this->db->where( 'MXC_ID_CAR', $id_cargo );
            $this->db->delete( 'cimberton_mxc_menuxcargo' );
        }

        return true;
    }

    /**
     * Asignar permisos de navegacion a cargo seleccionado
     * @param  array  $permisos listado de permisos de navegacion seleccionados
     * @param  integer $id_cargo ID de cargo seleccionado
     * @param  string  $tipo     O = opcion, M = menu principal, S = sub menu
     * @return boolean resultado de la operacion
     */
    public function asignarPermisosNavegacion( $permisos = null, $id_cargo = 0, $tipo = "O" )
    {
        $data = null;
        $resultado = false;
        if ( $tipo == "O" ) {
            foreach ( $permisos as $permiso ) {
                $data = array(
                    'OXC_ID_OPC' => $permiso,
                    'OXC_ID_CAR' => $id_cargo
                    );

                $resultado = $this->db->insert( 'cimberton_oxc_opcionxcargo', $data );
            }
        } else if ( $tipo == "S" ) {
            foreach ( $permisos as $permiso ) {
                $data = array(
                    'OXC_ID_OPC' => $permiso,
                    'OXC_ID_CAR' => $id_cargo
                    );

                $resultado = $this->db->insert( 'cimberton_oxc_opcionxcargo', $data );
                print_r($data); 
                print_r($resultado);
            }

        } else if ( $tipo == "M" ) {
            foreach ( $permisos as $permiso ) {
                $data = array(
                    'MXC_ID_MEN' => $permiso,
                    'MXC_ID_CAR' => $id_cargo
                    );
                $resultado = $this->db->insert( 'cimberton_mxc_menuxcargo', $data );   
            }
        }
        
        return $resultado;
    }

}

/* End of file model_administracion.php */
/* Location: ./application/models/model_administracion.php */