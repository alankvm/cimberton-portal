<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Design Pattern: Adapter, Facade
class Model_gerencia extends CI_Model {

    public $mes;
    public $year;
    public $acumulado; 

    public function __construct()
    {
        parent::__construct();
        $this->load->library('configuracion');
        $this->setConfiguracion();
        //Inicializando variables utilizadas
        $this->year      = $this->configuracion->year;
        $this->mes       = $this->configuracion->mes;
        $this->acumulado =  $this->configuracion->acumulado;
    }
    /**
     * Adquire Fecha actual y todos sus valores relacionados
     * @return object fecha actual
     */
    public function getFechaActual()
    {
        return $fecha_actual = new DateTime;
    }

    /**
     * Verifica y Obtiene listado de informacion para el tablero resumen de gerencia
     * @return object listado de informacion
     */
    public function getTableroResumenGerencia()
    {
        $resultado = false;

        if ( $this->configuracion->esPeriodo() ) {
            $resultado = $this->getTableroResumenGerenciaPersonalizado();
        } else if ( $this->configuracion->esPeriodoAcumulado() ) {
            $resultado = $this->getTableroResumenGerenciaAcumulado();
        }

        return $resultado;
    }

    /**
     * Subview filtros de periodo para tablero resumen
     * @return object subview renderizada
     */
    public function filtroPeriodos()
    {
        $data['listado_years'] = $this->getListadoYears();
        $filtro = $this->load->view( 'tablero/gerencia/filtro', $data, true );
        return $filtro;
    }

    /**
     * Obtiene informacion para tablero resumen de gerencia con fecha personzalida
     * @return object listado de informacion resumen
     */
    public function getTableroResumenGerenciaPersonalizado( )
    {
        $query_gerencia = "SELECT * 
        FROM cimberton_tblcontrol_empresa01
        WHERE NO_PERIODO = ? AND ANO_PERIODO = ?";
        
        //Obtener resultados
        $resultado = $this->db->query( $query_gerencia, array( $this->mes, $this->year ));

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false; 
    }

    /**
     * Obtiene listado de años almacenados en la lista de tableros consolidados
     * @return object listado años
     */
    public function getListadoYears()
    {
        $query_years = "SELECT DISTINCT ANO_PERIODO FROM cimberton_tblcontrol_empresa01";
        $resultado = $this->db->query( $query_years );

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false; 
    }

    /**
     * Obtiene listado de informacion para rel resumen gerencial acumulado
     * @param  integer $year Año seleccionado
     * @return object listado de elementos
     */
    public function getTableroResumenGerenciaAcumulado()
    {

        $query_gerencia = "SELECT CANAL, 
        DESCRIPCION_CANAL,
        SUM(ACTUAL) AS ACTUAL,
        SUM(PRONOSTICO) AS PRONOSTICO,
        CASE SUM(PRONOSTICO) WHEN 0 THEN 0 ELSE (ROUND(SUM(ACTUAL) / SUM(PRONOSTICO),2)) END AS CUMPLIMIENTO1,
        SUM(ANTERIOR) AS ANTERIOR,
        CASE SUM(ANTERIOR) WHEN 0 THEN 0 ELSE (ROUND(SUM(ACTUAL) / SUM(ANTERIOR),2)) END AS CUMPLIMIENTO2,
        SUM(COBRO) AS COBRO,
        SUM(COBRO_PRONOSTICO) AS COBRO_PRONOSTICO,
        CASE SUM(COBRO_PRONOSTICO) WHEN 0 THEN 0 ELSE (ROUND(SUM(COBRO) / SUM(COBRO_PRONOSTICO),2))  END AS CUMPLIMIENTO3
        FROM cimberton_tblcontrol_empresa01
        WHERE ANO_PERIODO = ?
        GROUP BY CANAL, DESCRIPCION_CANAL
        ORDER BY CANAL, ACTUAL DESC";

        $resultado = $this->db->query( $query_gerencia, array( $this->year ) );

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false; 
    }

    /**
     * Configura en session el periodo activo
     * @param integer $mes          Mes seleccionado
     * @param integer $year         Año seleccionado
     * @param boolean $es_acumulado Es reporte acumulado?
     */
    public function setPeriodoActivo( $mes = 0, $year = 0, $es_acumulado = false )
    {  
        /**
         * Refactorizado Best Practice: Generar archivo configuracion para datos persistentes
         * */
        $this->configuracion->setPeriodoActivo( $mes, $year, $es_acumulado );
        return true;
    }

    /**
     * Establecer configuracion inicial
     */
    public function setConfiguracion()
    {
        /**
         * Refactorizado Best Practice: Generar archivo configuracion para datos persistentes
         * */
        return $this->configuracion->setConfiguracion();
    }

    /**
     * Genera cabecera para el tablero resumen
     * @return string Cabecera de tablero resumen
     */
    public function getStringPeriodo()
    {
        $resultado = false;

        if ( isset( $this->mes ) && $this->mes != 0 && isset( $this->year ) && $this->year != 0 ) {
            $resultado = 'C. Imberton - Periodo '. $this->mes . '/' . $this->year . ' - ';
        } else if ( isset( $this->year ) && $this->year != 0 && isset( $this->acumulado ) && $this->acumulado != 0 ) {
            $resultado = 'C. Imberton - Periodo Acumulado/ ' . $this->year . ' - ';
        }
        return $resultado;
    }

}

/* End of file model_gerencia.php */
/* Location: ./application/models/model_gerencia.php */