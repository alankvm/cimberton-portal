<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_navegacion extends CI_Model {

    public $query_opciones;

    public function __construct()
    {
        parent::__construct();
        
    }

    /**
     * Contruye listado de menus de navegacion
     * @return string estructura de HTML para la navegacion
     */
    public function getMenuNavegacion()
    {
        //Inicializando menu principal
        $menu_navegacion = '<ul>';
        $opciones = $this->getOpcioneNavegacion();
        //Verificar si posee almenos 1 opcion de menu funcional.
        if ( !is_object( $opciones ) ) {
            redirect();
        }
        //Recorrer todos los resultados de la navegacion
        foreach ( $opciones->result() as $opcion ) {

            //Verificar si la opcion NO es menu principal
            if ( $opcion->es_padre != 1 ) {
                $menu_navegacion .= '<li>';
                $menu_navegacion .= '<a href="' . base_url() . $opcion->url . '" style="padding-left: 10px;">';
                $menu_navegacion .= '<span class="' . $opcion->icono . '"></span>';
                $menu_navegacion .= htmlentities ( $opcion->nombre ) . '</a></li>';    
            } else {
                //Si es, Menu principal
                $menu_navegacion .= '<li>';
                $menu_navegacion .= '<a href="#" style="padding-left: 10px;">';
                $menu_navegacion .= '<span class="' . $opcion->icono . '"></span>';
                $menu_navegacion .=  htmlentities ( $opcion->nombre ) . '</a>';
                //Obtener los detalles de los sub menus
                $detalles_opcion = explode( ', ', $opcion->opcion );
                //Verificar la cantidad de sub menus que posee
                if ( count( $detalles_opcion ) > 0 ) {
                    //Inicializar submenus
                    $menu_navegacion .= '<ul class="sub">';
                    //Recorrer listado de submenus
                    foreach ( $detalles_opcion as $sub_opcion ) {
                        //OPC_NOMBRE | OPC_NIVEL | OPC_URL | OPC_DESCRIPCION | ID_SUB_MENU | OPC_ICONO
                        $sub_menu = explode( '|', $sub_opcion );
                        $sub_opciones = '';
                        //Agregar sub menus solo si ID del submenu es mayor a Cero
                        if ( isset( $sub_menu[4] ) && $sub_menu[4] != 0 ) {
                            
                            $sub_opciones = $this->getSubOpcionesMenu( $sub_menu[4] );    
                        }
                        //Verificar si el submenu posee url
                        if ( isset( $sub_menu[2] ) && $sub_menu[2] != "" ) {
                            //Inicializar sub menus
                            $menu_navegacion .= '<li>';
                            $menu_navegacion .= '<a href="' . base_url( $sub_menu[2] ). '" style="padding-left: 10px;">';
                            $menu_navegacion .= '<span class="' . $sub_menu[5] . '"></span>';
                            //Utilizar HTMLENTITIES para convertir a caracteres equivalentes los caracteres especiales
                            $menu_navegacion .= htmlentities( $sub_menu[0] ) . '</a>';  
                            //Verificar si el listado de sub opciones del submenu son mayores a cero
                            if ( is_object( $sub_opciones ) && $sub_opciones->num_rows() > 0 ) {
                                //Inicializar listado de subopciones
                                $menu_navegacion .= '<ul class="sub">';
                                foreach ($sub_opciones->result() as $sub_opcion ) {
                                    //Agregar sub opcion
                                    $menu_navegacion .= '<li>';
                                    $menu_navegacion .= '<a href="' . base_url( $sub_opcion->url ) . '" ">';
                                    $menu_navegacion .= '<span class="icon16 icomoon-icon-arrow-right-2"></span>';
                                    $menu_navegacion .= htmlentities( $sub_opcion->nombre ) . '</a></li>';    
                                }
                                //Cerrando listado de subopciones
                                $menu_navegacion .= '</ul>';
                            } else  {
                                //Cerrando submenu
                                $menu_navegacion .= '</li>';    
                            }    
                        }
                    }//Finalizar recorridos de submenus
                    //Finalizar submenus
                    $menu_navegacion .= '</ul>';
                }
                //Finalizar listado de menu principal
                $menu_navegacion .= '</li>';
            }
        }
        
        //Finalizar Menu de Navegacion
        $menu_navegacion .= '</ul>';
        
        return $menu_navegacion;
        
    }

    /**
     * Obtiene listado de opciones de navegacion principal
     * @return object listado de opciones de navegacion 
     */
    public function getOpcioneNavegacion()
    {
        $this->query_opciones = "SELECT MEN_NOMBRE AS nombre,
        MEN_URL AS url,
        MEN_DESCRIPCION AS tooltip,
        MEN_ES_PADRE AS es_padre,
        MEN_ICONO AS icono,
        GROUP_CONCAT( CONCAT_WS('|', OPC_NOMBRE, OPC_NIVEL, OPC_URL, OPC_DESCRIPCION, OPC_ID, OPC_ICONO ) SEPARATOR ', ') AS opcion
        FROM cimberton_men_menu as menu
        INNER JOIN cimberton_mxc_menuxcargo AS mxc ON mxc.MXC_ID_MEN = MEN_ID AND MXC_ID_CAR = ?
        LEFT JOIN cimberton_opc_opcion AS opcion ON opcion.OPC_ID_MEN = menu.MEN_ID AND opcion.OPC_NIVEL = 1
        INNER JOIN cimberton_oxc_opcionxcargo AS oxc ON oxc.OXC_ID_OPC = opcion.OPC_ID AND oxc.OXC_ID_CAR = ?
        GROUP BY MEN_ID ASC";

        //Obtener el ID del cargo asignado
        $id_cargo = $this->session->userdata('cargo');
        //Filtrar listado de menus
        $opciones = $this->db->query( $this->query_opciones, array( $id_cargo, $id_cargo ) );

        return ( is_object( $opciones ) && $opciones->num_rows() > 0 ) ? $opciones : false;
    }

    /**
     * Obtiene listado de sub opciones de navegacion
     * @param  integer $id_sub_menu ID del sub menu seleccionado
     * @return object listado de subopciones de navegacion
     */
    public function getSubOpcionesMenu( $id_sub_menu )
    {
        
        $this->query_opciones = "SELECT OPC_NOMBRE AS nombre, 
        OPC_URL AS url,
        OPC_DESCRIPCION AS tooltip,
        OPC_ICONO AS icono
        FROM cimberton_opc_opcion AS opcion
        INNER JOIN cimberton_oxc_opcionxcargo AS oxc ON oxc.OXC_ID_OPC = opcion.OPC_ID AND oxc.OXC_ID_CAR = ? 
        WHERE opcion.OPC_PADRE = ?";

        //Obtener el ID del cargo asignado
        $id_cargo = $this->session->userdata('cargo');
        //Filtrar listado de menus
        $opciones = $this->db->query( $this->query_opciones, array( $id_cargo, $id_sub_menu ) );

        return ( is_object( $opciones ) && $opciones->num_rows() > 0 ) ? $opciones : false;
    }

    /**
     * Obtiene listado de menus principales
     * @return object listado de menus principales
     */
    public function getListadoMenusPrincipales()
    {
        $query_menus = "SELECT MEN_ID AS id,
        MEN_NOMBRE AS nombre,
        MEN_URL AS url,
        MEN_DESCRIPCION AS tooltip,
        MEN_ES_PADRE AS es_padre,
        MEN_ICONO AS icono
        FROM cimberton_men_menu as menu
        GROUP BY MEN_ID ASC";

        $resultado = $this->db->query( $query_menus );

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false;
    }

    /**
     * Almacena nuevo registro de menu principal en la base de datos
     * @return boolean resultado de la operacion
     */
    public function NuevoMenuPrincipal()
    {
        //Construyendo bloque de informacion a almacenar
        $es_padre = $this->input->post('es_padre');
        $data_menu = array(
            'MEN_NOMBRE'      => $this->input->post('nombre'),
            'MEN_URL'         => strtolower($this->input->post('url')),
            'MEN_DESCRIPCION' => $this->input->post('descripcion'),
            'MEN_ES_PADRE'    => ( isset(  $es_padre ) ? 1 : 0 ),
            'MEN_ICONO'       => $this->input->post('icono')
            );
        
        //Insertando nuevo menu principal
        $resultado = $this->db->insert( 'CIMBERTON_MEN_MENU', $data_menu );
        //Resultado de la operacion
        return $resultado;
    }

    /**
     * Agrega Nuevo Menu a la base de datos 
     * @return boolean resultado de la operacion
     */
    public function nuevoSubMenu()
    {
        //Construyendo bloque de informacion a almacenar
        $es_padre = $this->input->post('es_padre');
        $sub_menu = $this->input->post('submenu');
        $data_menu = array(
            'OPC_NOMBRE'      => $this->input->post('opcion'),
            'OPC_URL'         => strtolower($this->input->post('url')),
            'OPC_DESCRIPCION' => $this->input->post('descripcion'),
            'OPC_NIVEL'       => ( ( $sub_menu > 0 ) ? 2 : 1 ),
            'OPC_PADRE'       => ( ( isset( $sub_menu ) && $sub_menu > 0 ) ? $sub_menu : 0 ),
            'OPC_ICONO'       => $this->input->post('icono'),
            'OPC_ID_MEN'      => $this->input->post('principal')
            );

        //Insertando nuevo menu principal
        $resultado = $this->db->insert( 'CIMBERTON_OPC_OPCION', $data_menu );
        //Resultado de la operacion
        return $resultado;
    }

    /**
     * Obtiene listado de iconos para asignar a un menu en la aplicacion
     * @return object listado de iconos disponibles
     */
    public function getListadoIconosDisponibles()
    {
        $resultado = $this->db->get('CIMBERTON_ICO_ICONO');
        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false; 
    }
    
    /**
     * Obtiene listado de sub menus de nivel 1
     * @param  integer $id_menu_principal ID de menu principal seleccionado
     * @return json listado de sub menus
     */
    public function getListadoSubMenus( $id_menu_principal = 0 )
    {
        $query_sub_menus = "SELECT OPC_ID AS id, 
        OPC_NOMBRE AS nombre
        FROM cimberton_opc_opcion AS opcion
        WHERE opcion.OPC_ID_MEN = ? AND opcion.OPC_NIVEL = 1";

        
        //Filtrar listado de menus
        $resultado = $this->db->query( $query_sub_menus, array( $id_menu_principal ) );

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? json_encode( $resultado->result() ) : false; 
    }

}

/* End of file model_navegacion.php */
/* Location: ./application/models/model_navegacion.php */