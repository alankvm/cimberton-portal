<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_farmacia extends CI_Model {

    public $mes;
    public $year;
    public $acumulado;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('configuracion');
        //Adquirir Configuracion
        $this->configuracion->setConfiguracion();
        //Inicializando variables utilizadas
        $this->year      = $this->configuracion->year;
        $this->mes       = $this->configuracion->mes;
        $this->acumulado = $this->configuracion->acumulado;
    }

    /**
     * Obtener datos de Resumen para opcion Canal y Region
     * @return object resumen 
     */
    public function getDatosResumenCanalRegion()
    {
        $query_datos = "SELECT 
                            FECHA_ACTUALIZACION, DIAS_MES, DIAS_TRANSCURRIDOS
                        FROM
                            CIMBERTON_TBLCONTROL_DIAS
                        WHERE
                            ANO_PERIODO = ?
                                AND NO_PERIODO = ?
                                AND DIVISION = ?";

        $resultado = $this->db->query( $query_datos, array( $this->year, $this->mes, 1 ) );

        $falsa_salida = json_decode( json_encode( array('DIAS_MES' => 0, 'DIAS_TRANSCURRIDOS' => 0) ) );

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado->row() : $falsa_salida;
    }

    /**
     * Obtiene informacion del canal de distribucion para el area de farmacia
     * @return object reporte de periodo para el canal distribucion en farmacia
     */
    public function getObjetivoVentasCobros()
    {   
        $query_distribucion = "";
        $resultado = false;
        if ( isset( $this->mes ) && $this->mes > 0 ) {
            $query_distribucion = "SELECT 
                                    SUM(PRONOSTICO) AS PRONOSTICO, SUM(COBRO) AS COBRO
                                FROM
                                    CIMBERTON_TBLCONTROL_PV
                                WHERE
                                    ANO_PERIODO = ?
                                        AND NO_PERIODO = ?";
            $resultado = $this->db->query( $query_distribucion, array( $this->year, $this->mes ) );
        } else {
            $query_distribucion = "SELECT 
                                        SUM(PRONOSTICO) AS PRONOSTICO, SUM(COBRO) AS COBRO
                                    FROM
                                        CIMBERTON_TBLCONTROL_PV
                                    WHERE
                                        ANO_PERIODO = ?";
            $resultado = $this->db->query( $query_distribucion, array( $this->year ) );
        }
        
        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false;
    }

    /**
     * Obtiene resumen de ventas por el canal de distribucion
     * @return object resumen de ventas
     */
    public function getVentasCanalDistribucion()
    {
        $query_ventas = "";
        $resultado = false;
        if ( $this->configuracion->esPeriodo() ) {
            $query_ventas = "SELECT 
                            DESCRIPCION_CANAL,
                            ACTUAL,
                            PRONOSTICO,
                            CUMPLIMIENTO1 * 100 AS CUMPLIMIENTO1,
                            ANTERIOR,
                            CUMPLIMIENTO2 * 100 AS CUMPLIMIENTO2,
                            COBRO,
                            COBRO_PRONOSTICO,
                            CUMPLIMIENTO3 * 100 AS CUMPLIMIENTO3
                        FROM
                            CIMBERTON_TBLCONTROL_FARMA02
                        WHERE
                            ANO_PERIODO = ?
                                AND NO_PERIODO = ?
                        ORDER BY ACTUAL DESC";
            $resultado = $this->db->query( $query_ventas, array( $this->year, $this->mes ) );

        } else if ( $this->configuracion->esPeriodoAcumulado() ) {
            $query_ventas = "SELECT 
                                DESCRIPCION_CANAL,
                                SUM(ACTUAL) AS ACTUAL,
                                SUM(PRONOSTICO) AS PRONOSTICO,
                                CASE SUM(PRONOSTICO)
                                    WHEN 0 THEN 0
                                    ELSE (ROUND(SUM(ACTUAL) / SUM(PRONOSTICO), 2)) * 100
                                END AS CUMPLIMIENTO1,
                                SUM(ANTERIOR) AS ANTERIOR,
                                CASE SUM(ANTERIOR)
                                    WHEN 0 THEN 0
                                    ELSE (ROUND(SUM(ACTUAL) / SUM(ANTERIOR), 2)) * 100
                                END AS CUMPLIMIENTO2,
                                SUM(COBRO) AS COBRO,
                                SUM(COBRO_PRONOSTICO) AS COBRO_PRONOSTICO,
                                CASE SUM(COBRO_PRONOSTICO)
                                    WHEN 0 THEN 0
                                    ELSE (ROUND(SUM(COBRO) / SUM(COBRO_PRONOSTICO), 2)) * 100
                                END AS CUMPLIMIENTO3
                            FROM
                                CIMBERTON_TBLCONTROL_FARMA02
                            WHERE
                                ANO_PERIODO = ?
                            GROUP BY DESCRIPCION_CANAL
                            ORDER BY ACTUAL DESC";

            $resultado = $this->db->query( $query_ventas, array( $this->year ) );
        }

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false;
    }

    /**
     * Obtiene periodo para titulo
     * @return string titulo de periodo
     */
    public function getStringPeriodo()
    {
        return "C. Imberton - Periodo " . ( isset( $this->mes ) ? $this->mes : "Acumulado" ) . "/" . $this->year ;
    }

    /**
     * Obtiene resumen del canal de venta privado
     * @return object resumen de venta privada
     */
    public function getVentasCanalPrivado()
    {
        $resultado = false;
        $query_privado = "";

        if ( $this->configuracion->esPeriodo() ) {
            $query_privado = "SELECT 
                                DESCRIPCION_CANAL,
                                ACTUAL,
                                PRONOSTICO,
                                CUMPLIMIENTO1 * 100 AS CUMPLIMIENTO1,
                                ANTERIOR,
                                CUMPLIMIENTO2 * 100 AS CUMPLIMIENTO2,
                                COBRO,
                                COBRO_PRONOSTICO,
                                CUMPLIMIENTO3 * 100 AS CUMPLIMIENTO3
                            FROM
                                CIMBERTON_TBLCONTROL_FARMA01
                            WHERE
                                ANO_PERIODO = ?
                                    AND NO_PERIODO = ?
                            ORDER BY ACTUAL DESC";
            $resultado = $this->db->query( $query_privado, array( $this->year, $this->mes ) );
            
        } else if ( $this->configuracion->esPeriodoAcumulado() ) {
            $query_privado = "SELECT 
                                DESCRIPCION_CANAL,
                                SUM(ACTUAL) AS ACTUAL,
                                SUM(PRONOSTICO) AS PRONOSTICO,
                                CASE SUM(PRONOSTICO)
                                    WHEN 0 THEN 0
                                    ELSE (ROUND(SUM(ACTUAL) / SUM(PRONOSTICO), 2)) * 100
                                END AS CUMPLIMIENTO1,
                                SUM(ANTERIOR) AS ANTERIOR,
                                CASE SUM(ANTERIOR)
                                    WHEN 0 THEN 0
                                    ELSE (ROUND(SUM(ACTUAL) / SUM(ANTERIOR), 2)) * 100
                                END AS CUMPLIMIENTO2,
                                SUM(COBRO) AS COBRO,
                                SUM(COBRO_PRONOSTICO) AS COBRO_PRONOSTICO,
                                CASE SUM(COBRO_PRONOSTICO)
                                    WHEN 0 THEN 0
                                    ELSE (ROUND(SUM(COBRO) / SUM(COBRO_PRONOSTICO), 2)) * 100
                                END AS CUMPLIMIENTO3
                            FROM
                                CIMBERTON_TBLCONTROL_FARMA01
                            WHERE
                                ANO_PERIODO = ?
                            GROUP BY DESCRIPCION_CANAL
                            ORDER BY ACTUAL DESC";

            $resultado = $this->db->query( $query_privado, array( $this->year ) );
        }

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false;
    }

    /**
     * Obtiene totales de las resumenes seleccionados
     * @param  object $data colleccion de datos a sumar
     * @return object listado de totales por columna
     */
    public function getTotales( $data = object )
    {
        //Crear objecto anonimo
        $total = (object) array();

        //Fetch filas 
        foreach ($data->result_array() as $fila ) {

            //Crear las propiedades del objeto a partir de los nombres de los indices de la fila
            foreach ($fila as $key => $value) {
                //Asociar el valor del indice con el valor de la propiedad
                $total->{$key} += $value; 
            }
        }
        //Enviar totales
        return ( is_object( $total ) ) ? $total : false;
    }

    /**
     * Obtiene los totales del resumen seleccionado
     * @param  object  $data listado de elementos
     * @param  integer $eval cantidad a evaluar
     * @return object totales
     */
    public function getTotalesCondicionales( $data = object, $eval = 0, $opcion = "")
    {
        
        //Crear objecto anonimo
        $total = (object) array();
        if ( $opcion == "mayor" ) {
            //Fetch filas 
            foreach ( $data->result_array() as $fila ) {
                //Evaluar 
                if ( $fila['ACTUAL'] >= $eval ) {
                    //Crear las propiedades del objeto a partir de los nombres de los indices de la fila
                    foreach ($fila as $key => $value) {
                        //Asociar el valor del indice con el valor de la propiedad
                        $total->{$key} += $value; 
                    }    
                }
                
            }    
        } else if ( $opcion == "menor" ) {
            //Fetch filas 
            foreach ( $data->result_array() as $fila ) {
                //Evaluar 
                if ( $fila['ACTUAL'] < $eval ) {
                    //Crear las propiedades del objeto a partir de los nombres de los indices de la fila
                    foreach ($fila as $key => $value) {
                        //Asociar el valor del indice con el valor de la propiedad
                        $total->{$key} += $value; 
                    }    
                }
                
            }
        }
        
        //Enviar totales
        return ( is_object( $total ) ) ? $total : false;
    }

    /**
     * Obtiene resumen de ventas para el area de farmacia en el canal privado
     * @return object resumen de ventas
     */
    public function getResumenCanalPrivado()
    {
        $resultado = false;
        $qr = null;
        if ( $this->configuracion->esPeriodo() ) {
            $qr = "SELECT 
                    CODIGO_MARCA,
                    DESCRIPCION_MARCA,
                    ACTUAL,
                    PRONOSTICO,
                    CUMPLIMIENTO1 * 100 AS CUMPLIMIENTO1,
                    ANTERIOR,
                    CUMPLIMIENTO2 * 100 AS CUMPLIMIENTO2
                FROM
                    CIMBERTON_TBLCONTROL_FARMA04
                WHERE
                    ANO_PERIODO = ?
                        AND NO_PERIODO = ?
                        AND CODIGO_CANAL = 1
                        AND CODIGO_MARCA NOT IN ('218' , '141')
                UNION SELECT 
                    '218' AS CODIGO_MARCA,
                    'PROCTER & GAMBLE FARMACIA' AS DESCRIPCION_MARCA,
                    SUM(ACTUAL) AS ACTUAL,
                    SUM(PRONOSTICO) AS PRONOSTICO,
                    CASE SUM(PRONOSTICO)
                        WHEN 0 THEN 0
                        ELSE (ROUND(SUM(ACTUAL) / SUM(PRONOSTICO), 2) * 100)
                    END AS CUMPLIMIENTO1,
                    SUM(ANTERIOR) AS ANTERIOR,
                    CASE SUM(ANTERIOR)
                        WHEN 0 THEN 0
                        ELSE (ROUND(SUM(ACTUAL) / SUM(ANTERIOR), 2) * 100)
                    END AS CUMPLIMIENTO2
                FROM
                    CIMBERTON_TBLCONTROL_FARMA04
                WHERE
                    ANO_PERIODO = ?
                        AND NO_PERIODO = ?
                        AND CODIGO_CANAL = 1
                        AND CODIGO_MARCA IN ('218' , '141')
                ORDER BY ACTUAL DESC";
            $resultado = $this->db->query( $qr, array( $this->year, $this->mes, $this->year, $this->mes) );
        } else  {
            $qr = "SELECT 
                        CODIGO_MARCA,
                        DESCRIPCION_MARCA,
                        SUM(ACTUAL) AS ACTUAL,
                        SUM(PRONOSTICO) AS PRONOSTICO,
                        CASE SUM(PRONOSTICO)
                            WHEN 0 THEN 0
                            ELSE (ROUND(SUM(ACTUAL) / SUM(PRONOSTICO), 2)) * 100
                        END AS CUMPLIMIENTO1,
                        SUM(ANTERIOR) AS ANTERIOR,
                        CASE SUM(ANTERIOR)
                            WHEN 0 THEN 0
                            ELSE (ROUND(SUM(ACTUAL) / SUM(ANTERIOR), 2)) * 100
                        END AS CUMPLIMIENTO2
                    FROM
                        CIMBERTON_TBLCONTROL_FARMA04
                    WHERE
                        ANO_PERIODO = ?
                            AND CODIGO_CANAL = 1
                            AND CODIGO_MARCA NOT IN ('218' , '141')
                    GROUP BY CODIGO_MARCA , DESCRIPCION_MARCA 
                    UNION SELECT 
                        '218' AS CODIGO_MARCA,
                        'PROCTER & GAMBLE FARMACIA' AS DESCRIPCION_MARCA,
                        SUM(ACTUAL) AS ACTUAL,
                        SUM(PRONOSTICO) AS PRONOSTICO,
                        CASE SUM(PRONOSTICO)
                            WHEN 0 THEN 0
                            ELSE (ROUND(SUM(ACTUAL) / SUM(PRONOSTICO), 2) * 100)
                        END AS CUMPLIMIENTO1,
                        SUM(ANTERIOR) AS ANTERIOR,
                        CASE SUM(ANTERIOR)
                            WHEN 0 THEN 0
                            ELSE (ROUND(SUM(ACTUAL) / SUM(ANTERIOR), 2) * 100)
                        END AS CUMPLIMIENTO2
                    FROM
                        CIMBERTON_TBLCONTROL_FARMA04
                    WHERE
                        ANO_PERIODO = ?
                            AND CODIGO_CANAL = 1
                            AND CODIGO_MARCA IN ('218' , '141')
                    ORDER BY ACTUAL DESC";

            $resultado = $this->db->query( $qr, array( $this->year, $this->year ) );
        }

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false;
    }

    /**
     * Obtiene listado de sub marcas correspondientes a PyG
     * @return object Listado de submarcas
     */
    public function getMarcasPyG()
    {
        $resultado = false;
        $query_pyg = "";
        if ( $this->configuracion->esPeriodo() ) {
            $query_pyg = "SELECT 
                            CODIGO_MARCA,
                            DESCRIPCION_MARCA,
                            ACTUAL,
                            PRONOSTICO,
                            CUMPLIMIENTO1 * 100 AS CUMPLIMIENTO1,
                            ANTERIOR,
                            CUMPLIMIENTO2 * 100 AS CUMPLIMIENTO2
                        FROM
                            CIMBERTON_TBLCONTROL_FARMA04
                        WHERE
                            ANO_PERIODO = ?
                                AND NO_PERIODO = ?
                                AND CODIGO_CANAL = 1
                                AND CODIGO_MARCA IN ('218' , '141')
                        ORDER BY ACTUAL DESC";

            $resultado = $this->db->query( $query_pyg, array( $this->year, $this->mes ) );
        } else  {
            $query_pyg = "SELECT 
                            CODIGO_MARCA,
                            DESCRIPCION_MARCA,
                            SUM(ACTUAL) AS ACTUAL,
                            SUM(PRONOSTICO) AS PRONOSTICO,
                            CASE SUM(PRONOSTICO)
                                WHEN 0 THEN 0
                                ELSE (ROUND(SUM(ACTUAL) / SUM(PRONOSTICO), 2)) * 100
                            END AS CUMPLIMIENTO1,
                            SUM(ANTERIOR) AS ANTERIOR,
                            CASE SUM(ANTERIOR)
                                WHEN 0 THEN 0
                                ELSE (ROUND(SUM(ACTUAL) / SUM(ANTERIOR), 2)) * 100
                            END AS CUMPLIMIENTO2
                        FROM
                            CIMBERTON_TBLCONTROL_FARMA04
                        WHERE
                            ANO_PERIODO = ?
                                AND CODIGO_CANAL = 1
                                AND CODIGO_MARCA IN ('218' , '141')
                        GROUP BY CODIGO_MARCA , DESCRIPCION_MARCA
                        ORDER BY ACTUAL DESC";

            $resultado = $this->db->query( $query_pyg, array( $this->year ) );
        }

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false;
    }

    /**
     * Obtiene resumen de ventas por licitacion
     * @return object resumen de ventas por licitacion
     */
    public function getResumenLicitaciones()
    {
        $resultado = FALSE;
        $query     = FALSE;

        if ( $this->configuracion->esPeriodo() ) {
            $query = "SELECT 
                        CODIGO_MARCA,
                        DESCRIPCION_MARCA,
                        ACTUAL,
                        PRONOSTICO,
                        CUMPLIMIENTO1 * 100 AS CUMPLIMIENTO1,
                        ANTERIOR,
                        CUMPLIMIENTO2 * 100 AS CUMPLIMIENTO2
                    FROM
                        CIMBERTON_TBLCONTROL_FARMA04
                    WHERE
                        ANO_PERIODO = ?
                            AND NO_PERIODO = ?
                            AND CODIGO_CANAL = 2
                    ORDER BY ACTUAL DESC";

            $resultado = $this->db->query( $query, array( $this->year, $this->mes ) );
        } else {
            $query = "SELECT 
                        CODIGO_MARCA,
                        DESCRIPCION_MARCA,
                        SUM(ACTUAL) AS ACTUAL,
                        SUM(PRONOSTICO) AS PRONOSTICO,
                        CASE SUM(PRONOSTICO)
                            WHEN 0 THEN 0
                            ELSE (ROUND(SUM(ACTUAL) / SUM(PRONOSTICO), 2)) * 100
                        END AS CUMPLIMIENTO1,
                        SUM(ANTERIOR) AS ANTERIOR,
                        CASE SUM(ANTERIOR)
                            WHEN 0 THEN 0
                            ELSE (ROUND(SUM(ACTUAL) / SUM(ANTERIOR), 2)) * 100
                        END AS CUMPLIMIENTO2
                    FROM
                        CIMBERTON_TBLCONTROL_FARMA04
                    WHERE
                        ANO_PERIODO = ?
                            AND CODIGO_CANAL = 2
                    GROUP BY CODIGO_MARCA , DESCRIPCION_MARCA
                    ORDER BY ACTUAL DESC";
            $resultado = $this->db->query( $query, array( $this->year ) );
        }

        return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false;
    }

    /**
     * Obtiene estadisticas de venta por Vendedor
     * @return object resumen estadistico
     */
    public function getResumenVendedores()
    {
        $resultado = FALSE;
        $query     = FALSE;

        if ( $this->configuracion->esPeriodo() ) {
            $query = "SELECT 
                        CODIGO_VENDEDOR,
                        NOMBRE_VENDEDOR,
                        ACTUAL,
                        PRONOSTICO,
                        CUMPLIMIENTO1 * 100 AS CUMPLIMIENTO1,
                        ANTERIOR,
                        CUMPLIMIENTO2 * 100 AS CUMPLIMIENTO2,
                        COBRO,
                        COBRO_PRONOSTICO,
                        CUMPLIMIENTO3 * 100 AS CUMPLIMIENTO3
                    FROM
                        CIMBERTON_TBLCONTROL_FARMA03
                    WHERE
                        ANO_PERIODO = ?
                            AND NO_PERIODO = ?
                    ORDER BY ACTUAL DESC";

            $resultado = $this->db->query( $query, array( $this->year, $this->mes ) );
        } else {
            $query = "";
        }

       return ( is_object( $resultado ) && $resultado->num_rows() > 0 ) ? $resultado : false; 
    }

}

/* End of file model_farmacia.php */
/* Location: ./application/models/model_farmacia.php */