<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_alertas extends CI_Model {

    public $variable;

    public function __construct()
    {
        parent::__construct();
        
    }

    public function setAlerta( $parametros = array() )
    {
        $segmento = false;
        $alerta = "";
        $resultado = "";
        
        
        if ( isset( $parametros['segmento'] ) && $parametros['segmento'] != "" ) {
            $segmento = $this->uri->segment( $parametros['segmento'] );
        }

        if ( $segmento === "1" ) {
            $alerta .= '<div class="alert alert-success">';
            $alerta .= '<button type="button" class="close" data-dismiss="alert">×</button>';
            $alerta .= '<strong>En hora buena!</strong> ' . ( isset ( $parametros['exito'] ) ? $parametros['exito'] : 'Registro Almacenado Exitosamente');
            $alerta .= '.</div>';
        } else if ( $segmento === "0" ) {
            $alerta .= '<div class="alert alert-error">';
            $alerta .= '<button type="button" class="close" data-dismiss="alert">×</button>';
            $alerta .= '<strong>...Oh no!</strong> ' . ( isset ( $parametros['error'] ) ? $parametros['error'] : 'Tuvimos un inconveniente, por favor intentalo de nuevo');
            $alerta .= '.</div>';
        }
        return $alerta;        
    }

}

/* End of file model_alertas.php */
/* Location: ./application/models/model_alertas.php */