<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class model_autentificacion extends CI_Model {

    public $variable;

    public function __construct()
    {
        parent::__construct();
        
    }
    /**
     * Genera password inicial y actualiza informacion de acceso segun el usuario ingresado
     * @param  integer $id_usuario ID de usuario
     * @param  array   $datos_post datos de ingreso al usuario
     * @return boolean resultado de la operacion
     */
    public function establecerPasswordInicial( $id_usuario  = 0, $datos_post = array() )
    {
        //Generar Password Generico Inicial
        $codigo = md5( strtolower( $datos_post['USU_USUARIO'] ) );
        
        //Actualizar datos
        $this->db->where( 'USU_ID', $id_usuario );
        $resultado = $this->db->update( 'cimberton_usu_usuario', array( 'USU_PASSWORD' => $codigo ) );

        return $resultado;
    }

    /**
     * Actualiza Password del usuario seleccionado
     * @param  array  $datos_perfil Informacion del usuario seleccionado
     * @return boolean resultado de la operacion
     */
    public function actualizarPassword( $datos_perfil = array() )
    {
        //Inicializar datos
        $id_usuario       = $datos_perfil['id_usuario'];
        $current_password = md5( $datos_perfil['old_password'] );
        $new_password     = md5( $datos_perfil['new_password'] );
        $resultado        = 0;

        //Realizar consulta para verificar los datos ingresados
        $prueba_acceso = $this->db->where( 'USU_ID', $id_usuario )->get( 'cimberton_usu_usuario' )->row();

        //Verificar si digito el password actual correctamente
        if ( $current_password == $prueba_acceso->USU_PASSWORD ) {
            //Continuar con la actualizacion de password
            $data_actualizar = array(
                'USU_PASSWORD' => $new_password
                );
            $this->db->where( 'USU_ID', $id_usuario )->update( 'cimberton_usu_usuario', $data_actualizar );
            $resultado = TRUE;
        } 

        return $resultado;
    }

}

/* End of file model_autentificacion.php */
/* Location: ./application/models/model_autentificacion.php */