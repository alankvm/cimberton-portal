
<div class="row-fluid">

    <div class="span12">
        <?php
        //Construyendo datos para el feedback 
        $parametros = array(
            'segmento' => 4,
            'exito'    => 'La informaci&oacute;n de t&uacute; perfil ha sido actualizada exitosamente',
            'error'    => 'Contrase&ntilde;a actual no coincide, por favor intentalo de nuevo'
            );
        //Imprimiendo alerta resultante de la operacion
        print $this->model_alertas->setAlerta( $parametros );
            
        ?>
        <div class="page-header">
            <h4>Editar Perfil</h4>
        </div>

        
<?php echo form_open( 'autentificacion/perfil/actualizar', array( 'class' => 'form-horizontal seperator', 'id' => 'frm_perfil' ) ); ?>
            <?php echo form_hidden( 'id_usuario', $this->session->userdata( 'codigo' ) ); ?>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="usuario">Usuario:</label>
                        <input class="span4" id="usuario" name="usuario" type="text" disabled="disabled" value="<?=$informacion->USU_USUARIO?>" />
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="nombres">Nombres:</label>
                        <input class="span4" id="nombres" type="text" disabled="disabled" value="<?=$informacion->USU_NOMBRE?>" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="apellidos">Apellidos:</label>
                        <input class="span4" id="apellidos" type="text" disabled="disabled" value="<?=$informacion->USU_APELLIDO?>" />
                    </div>
                </div>
            </div>
            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="normal">Contrase&ntilde;a actual:</label>
                        <div class="span4 controls">
                            <input class="span12" id="old_password" name="old_password" type="password" placeholder="Introduce t&uacute; contrase&ntilde;a actual" value="" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-row row-fluid">
                <div class="span12">
                    <div class="row-fluid">
                        <label class="form-label span3" for="normal">Contrase&ntilde;a nueva:</label>
                        <div class="span4 controls">
                            <input class="span12" id="new_password" name="new_password" type="password" placeholder="Introduce la nueva contrase&ntilde;a" value="" />
                            <input class="span12" id="confirm_new_password" name="confirm_new_password" type="password" placeholder="Repite la nueva contrase&ntilde;a" />
                        </div>
                    </div>
                </div>
            </div>
  
            <div class="form-row row-fluid">        
                <div class="span12">
                    <div class="row-fluid">
                        <div class="form-actions">
                        <div class="span3"></div>
                        <div class="span4 controls">
                            <button type="submit" class="btn btn-info marginR10">Actualizar Perfil</button>
                            <button type="reset" class="btn btn-danger">Cancelar</button>
                        </div>
                        </div>
                    </div>
                </div>   
            </div>

    <?php echo form_close(); ?>
      
    </div><!-- End .span12 -->

</div><!-- End .row-fluid -->