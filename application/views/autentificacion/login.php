<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?=$this->config->item('entidad')?> | <?php ( isset( $titulo ) ? print $titulo : print "Bienvenido" ) ?></title>
    <meta name="author" content="Grupo Satelite" />
    <meta name="description" content="Autentificacion de usuarios" />
    <meta name="keywords" content="Formulario de Identifiacion" />
    <meta name="application-name" content="Portal Corporativo - C. Imberton - El Salvador" />

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Le styles -->
    <link href="<?=base_url('css/bootstrap/bootstrap.css')?>" rel="stylesheet" />
    <link href="<?=base_url('css/bootstrap/bootstrap-responsive.css')?>" rel="stylesheet" />
    <link href="<?=base_url('css/supr-theme/jquery.ui.supr.css')?>" rel="stylesheet" type="text/css"/>
    <link href="<?=base_url('css/icons.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('plugins/forms/uniform/uniform.default.css')?>" type="text/css" rel="stylesheet" />

    <!-- Main stylesheets -->
    <link href="<?=base_url('css/main.css')?>" rel="stylesheet" type="text/css" /> 
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?=base_url('js/html5/shiv.js')?>"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="icon" href="<?=base_url('images/favicon.ico')?>" type="image/x-icon" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=base_url('images/apple-touch-icon-144-precomposed.png')?>" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=base_url('images/apple-touch-icon-114-precomposed.png')?>" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=base_url('images/apple-touch-icon-72-precomposed.png')?>" />
    <link rel="apple-touch-icon-precomposed" href="<?=base_url('images/apple-touch-icon-57-precomposed.png')?>" />

    </head>
      
    <body class="loginPage">

    <div class="container-fluid">

        <div id="header">

            <div class="row-fluid">

                <div class="navbar">
                    <div class="navbar-inner">
                      <div class="container">
                            <a class="brand" href="#">
                            <img src="<?=base_url('images/header/logo-transparente-noname.png')?>" alt="C Imberton S.A de C.V" class="image" style="height: 66px;">
                            <?=$this->config->item('entidad')?></a>
                      </div>
                    </div><!-- /navbar-inner -->
                </div><!-- /navbar -->
                

            </div><!-- End .row-fluid -->

        </div><!-- End #header -->

    </div><!-- End .container-fluid -->    

    <div class="container-fluid">
            
        <div class="loginContainer">
            
            <form class="form-horizontal" action="<?=base_url('autentificacion/login/validar')?>" id="loginForm" method="POST" >
                <?php echo form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash()); ?>
                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span12" for="username">
                                Usuario:
                                <span class="icon16 icomoon-icon-user-3 right gray marginR10"></span>
                            </label>
                            <input class="span12" id="username" type="text" name="username" value="" placeholder="Nombre de Usuario" />
                        </div>
                    </div>
                </div>

                <div class="form-row row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <label class="form-label span12" for="password">
                                Contrase&ntilde;a:
                                <span class="icon16 icomoon-icon-locked right gray marginR10"></span>
                            </label>
                            <input class="span12 tip" id="password" type="password" name="password" value="" title="Introduzca su contrase&ntilde;a" placeholder="Digite su contrase&ntilde;a"/>
                        </div>
                    </div>
                </div>
                <div class="form-row row-fluid">                       
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="form-actions">
                            <div class="span12 controls">
                                
                                <button type="submit" class="btn btn-info right" id="loginBtn"><span class="icon16 icomoon-icon-enter white"></span> Entrar</button>
                            </div>
                            </div>
                        </div>
                    </div> 
                </div>

            </form>

        </div>
        <?php if($this->session->flashdata('error')) : ?>
                <div class="alert alert-error" style="margin: 10% auto; width: 38%;">
                    <strong>Usuario o Contrase&ntilde;a incorrecto!</strong> Por favor, verifica los datos ingresados al sistema.
                </div>
            <?php endif; ?>
    </div><!-- End .container-fluid -->

    <!-- Le javascript
    ================================================== -->
    <!-- Important plugins put in all pages -->
    <script type="text/javascript" src="<?=base_url('js/jquery/jquery-1.7.2.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('plugins/misc/touch-punch/jquery.ui.touch-punch.min.js')?>"></script>  
    <script type="text/javascript" src="<?=base_url('plugins/misc/ios-fix/ios-orientationchange-fix.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('plugins/forms/validate/jquery.validate.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('plugins/forms/validate/localization/messages_es.js')?>"></script>

     <script type="text/javascript">
        // document ready function
        $(document).ready(function ($) {
            //$("input, textarea, select").not('.nostyle').uniform();
            $("#loginForm").validate({
                rules: {
                    username: {
                        required: true,
                        minlength: 4
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }  
                },
                messages: {
                    username: {
                        required: "Por favor, ingrese su nombre de usuario",
                        minlength: "Su usuario debe ser mayor o igual a 3 caracteres"
                    },
                    password: {
                        required: "Por favor, ingrese su contrase&ntilde;a",
                        minlength: "Su contrase&ntilde;a, debe ser mayor o igual a 4 caracteres"
                    }
                }   
            });
        });
    </script>

    </body>
</html>
