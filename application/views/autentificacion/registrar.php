<div class="row-fluid">

    <div class="span12">

        <div class="box">

            <div class="title">

                <h4> 
                    <span>Registro de Usuarios</span>
                </h4>
                
            </div>
            <div class="content">
               
                <form class="form-horizontal" action="#">
                    
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="nombre">Nombres</label>
                                <input class="span8" id="nombre" name="nombre" type="text" placeholder="Nombres del Usuario"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="apellido">Apellidos</label>
                                <input class="span8" id="apellido" name="apellido" type="text" placeholder="Apellidos del Usuario" />
                            </div>
                        </div>
                    </div>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="normal">Usuario</label>
                                <input class="span8" id="normalInput" type="text" placeholder="Nombre de Acceso"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                       <button type="submit" class="btn btn-primary">Guardar</button>
                       <button type="reset" class="btn">Cancelar</button>
                    </div>
                                                            

                </form>
             
            </div>

        </div><!-- End .box -->

    </div><!-- End .span12 -->

</div><!-- End .row-fluid -->