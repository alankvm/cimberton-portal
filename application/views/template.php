<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?=$this->config->item('entidad')?> | <?php ( isset( $titulo ) ? print $titulo : print "Bienvenido" ) ?></title>
    <meta name="author" content="Grupo Satelite" />
    <meta name="description" content="Departamento de Ventas" />
    <meta name="keywords" content="" />
    <meta name="application-name" content="Portal Corporativo - C. Imberton - El Salvador" />

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="text/javascript">
        //adding load class to body and hide page
        document.documentElement.className += 'loadstate';
    </script>
    
    <!-- Core stylesheets do not remove -->
    <link href="<?=base_url('css/bootstrap/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('css/bootstrap/bootstrap-responsive.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('css/supr-theme/jquery.ui.supr.css')?>" rel="stylesheet" type="text/css"/>
    <link href="<?=base_url('css/icons.css')?>" rel="stylesheet" type="text/css" />


    <!-- Plugins stylesheets -->
    <link href="<?=base_url('plugins/misc/qtip/jquery.qtip.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('plugins/misc/pnotify/jquery.pnotify.default.css')?>" type="text/css" rel="stylesheet" />
    <link href="<?=base_url('plugins/misc/fullcalendar/fullcalendar.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?=base_url('plugins/misc/search/tipuesearch.css')?>" type="text/css" rel="stylesheet" />
    <link href="<?=base_url('plugins/forms/inputlimiter/jquery.inputlimiter.css')?>" type="text/css" rel="stylesheet" />
    <link href="<?=base_url('plugins/forms/ibutton/jquery.ibutton.css')?>" type="text/css" rel="stylesheet" />
    <link href="<?=base_url('plugins/forms/uniform/uniform.default.css')?>" type="text/css" rel="stylesheet" />
    <link href="<?=base_url('plugins/forms/color-picker/color-picker.css')?>" type="text/css" rel="stylesheet" />
    <link href="<?=base_url('plugins/forms/select/select2.css')?>" type="text/css" rel="stylesheet" />
    <link href="<?=base_url('plugins/forms/validate/validate.css')?>" type="text/css" rel="stylesheet" />
    <link href="<?=base_url('plugins/forms/smartWizzard/smart_wizard.css')?>" type="text/css" rel="stylesheet" />
    <link href="<?=base_url('plugins/tables/responsive-tables/responsive-tables.css')?>" type="text/css" rel="stylesheet" />

    <!-- Main stylesheets -->
    <link href="<?=base_url('css/main.css')?>" rel="stylesheet" type="text/css" /> 

    <!-- Custom stylesheets ( Put your own changes here ) -->
    <link href="<?=base_url('css/custom.css')?>" rel="stylesheet" type="text/css" /> 
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?=base_url('js/html5/shiv.js')?>"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="icon" href="<?=base_url('images/favicon.ico')?>" type="image/x-icon" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=base_url('images/apple-touch-icon-144-precomposed.png')?>" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=base_url('images/apple-touch-icon-114-precomposed.png')?>" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=base_url('images/apple-touch-icon-72-precomposed.png')?>" />
    <link rel="apple-touch-icon-precomposed" href="<?=base_url('images/apple-touch-icon-57-precomposed.png')?>" />
    
    

    </head>
    <body>
    <!-- loading animation -->
    <div id="qLoverlay"></div>
    <div id="qLbar"></div>
        
    <div id="header">

        <div class="navbar">
            <div class="navbar-inner">
              <div class="container-fluid">
                <a class="brand" href="<?=base_url('panel/control')?>" style="margin-right: 12px; margin-botton: 10px">
                  <img src="<?=base_url('images/header/logo-transparente-noname.png')?>" alt="C Imberton S.A de C.V" class="image" style="height: 66px;">
                  C. Imberton
                  <span class="slogan"> S.A de C.V</span>
                  </a>
                <div class="nav-no-collapse">
                  
                    <ul class="nav pull-right usernav">
                        <!-- <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="icon16 icomoon-icon-bell-2"></span><span class="notification">3</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="menu">
                                    <ul class="notif">
                                        <li class="header"><strong>Notifications</strong> (3) items</li>
                                        <li>
                                            <a href="#">
                                                <span class="icon"><span class="icon16 icomoon-icon-user-3"></span></span>
                                                <span class="event">1 User is registred</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="icon"><span class="icon16 icomoon-icon-comments-4"></span></span>
                                                <span class="event">Jony add 1 comment</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="icon"><span class="icon16 icomoon-icon-new-2"></span></span>
                                                <span class="event">admin Julia added post with a long description</span>
                                            </a>
                                        </li>
                                        <li class="view-all"><a href="#">View all notifications <span class="icon16 icomoon-icon-arrow-right-8"></span></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li> -->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="txt">Bienvenido/a: <?=$this->session->userdata('usuario')?></span>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="menu">
                                    <ul>
                                        <li>
                                            <a href="<?=base_url('autentificacion/perfil')?>"><span class="icon16 icomoon-icon-user-3"></span>Editar Perfil</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="<?=base_url('autentificacion/login/cerrar')?>"><span class="icon16 icomoon-icon-exit"></span> Cerrar Sesi&oacute;n</a></li>
                    </ul>
                </div><!-- /.nav-collapse -->
              </div>
            </div><!-- /navbar-inner -->
          </div><!-- /navbar --> 

    </div><!-- End #header -->

    <div id="wrapper" class="page-wrap">
        <!--Responsive navigation button-->  
        <div class="resBtn">
            <a href="#"><span class="icon16 minia-icon-list-3"></span></a>
        </div>

        <!--Sidebar background-->
        <div id="sidebarbg"></div>
        <!--Sidebar content-->
        <div id="sidebar">

            <div class="sidenav" style="width: 100%;">

                <div class="sidebar-widget" style="margin: 2px 0 0 0;">
                    <h3 class="title" style="padding: 10px; height: 19px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Navegaci&oacute;n</h3>
                </div><!-- End .sidenav-widget -->

                <div class="mainnav">
                <?php 
                $opciones = $this->model_navegacion->getMenuNavegacion();
                if ( isset( $opciones) ) { print $opciones; }  ?>
                </div>
            </div><!-- End sidenav -->

        </div><!-- End #sidebar -->

        <!--Body content-->
        <div id="content" class="clearfix">
            <div class="contentwrapper"><!--Content wrapper-->

                <div class="heading">

                    <h3><?php ( isset( $ubicacion ) ? print $ubicacion: print "Inicio" ) ?></h3>                    

                    <ul class="breadcrumb">
                        <li>Usted est&aacute; aqu&iacute;:</li>
                        <li>
                              <span class="icon16 icomoon-icon-screen-2"></span>
                              <span class="divider">
                                 <span class="icon16 icomoon-icon-arrow-right-2"></span>
                              </span>
                        </li>
                        <li class="active"><?php ( isset( $ubicacion ) ? print $ubicacion: print "Inicio" ) ?></li>
                    </ul>

                </div><!-- End .heading-->

                <!-- Inicia Cuerpo Personalizado de la pagina -->

                <?php print ( isset( $content ) ? $content : "Proximamente!" ) ?>

                <!-- Finaliza Cuerpo Personalizado de la pagina -->
            </div><!-- End contentwrapper -->
        </div><!-- End #content -->
        
      </div><!-- End #wrapper -->
      <footer class="site-footer" >
         <p class="right" style="margin-right: 40px;">
         <br>
            <?=$this->config->item('entidad')?> &copy; <?=date('Y')?><br>
            <small>P&aacute;gina renderizada en <strong>{elapsed_time}</strong> segundos</small>
         </p>
      </footer>
    <!-- Le javascript
    ================================================== -->
    <!-- Important plugins put in all pages -->
    <script type="text/javascript" src="<?=base_url('js/jquery/jquery-1.7.2.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('js/bootstrap/bootstrap.js')?>"></script>  
    <script type="text/javascript" src="<?=base_url('js/underscore/underscore-min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('js/jquery.cookie.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('js/jquery.mousewheel.js')?>"></script>

    <!-- Misc plugins -->
    <script type="text/javascript" src="<?=base_url('plugins/misc/qtip/jquery.qtip.min.js')?>"></script><!-- Custom tooltip plugin -->
    <script type="text/javascript" src="<?=base_url('plugins/misc/totop/jquery.ui.totop.min.js')?>"></script> <!-- Back to top plugin -->

    <!-- Search plugin -->
    <script type="text/javascript" src="<?=base_url('plugins/misc/search/tipuesearch_set.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('plugins/misc/search/tipuesearch_data.js')?>"></script><!-- JSON for searched results -->
    <script type="text/javascript" src="<?=base_url('plugins/misc/search/tipuesearch.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('plugins/misc/pnotify/jquery.pnotify.min.js')?>"></script>
    

    <!-- Utils Plugin -->
    <script type="text/javascript" src="<?=base_url('js/utils.js')?>"></script>

    <!-- Important Place before main.js  -->
    <script type="text/javascript" src="<?=base_url('js/jquery/jquery.easing.1.3.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('js/jquery/jquery-ui-1.10.3.custom.min.js')?>"></script>
    <!-- plugins Necesarios -->
<?php 
    if ( isset( $scripts ) && count( $scripts ) > 0 ): 
        foreach ( $scripts as $script ) : ?>
    <script type="text/javascript" src="<?=base_url($script)?>"></script>
<?php   endforeach; 
    endif; ?>
    
    <!-- Fix plugins -->
    <script type="text/javascript" src="<?=base_url('plugins/fix/ios-fix/ios-orientationchange-fix.js')?>"></script>

    
    <!-- Unable touch for JQueryUI -->
    <script type="text/javascript" src="<?=base_url('plugins/fix/touch-punch/jquery.ui.touch-punch.min.js')?>"></script>

    <!-- Init plugins -->
    <script type="text/javascript" src="<?=base_url('js/main.js')?>"></script><!-- Core js functions -->
    <script type="text/javascript" src="<?=base_url('js/dashboard.js')?>"></script><!-- Init plugins only for page -->


    </body>
</html>
