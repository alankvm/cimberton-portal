<div class="row-fluid">

    <div class="span12">
        <?php
        //Construyendo datos para el feedback 
        $parametros = array(
            'segmento' => 4,
            'exito'    => 'El menu principal ha sido guardado exitosamente',
            'error'    => 'Tuvimos un inconveniente, por favor intentalo de nuevo'
            );
        //Imprimiendo alerta resultante de la operacion
        print $this->model_alertas->setAlerta( $parametros );
            
        ?>
        <div class="box">

            <div class="title">

                <h4> 
                    <span>Creaci&oacute;n de Menus</span>
                </h4>
                
            </div>
            <div class="content">
               
                <form class="form-horizontal" action="<?=base_url('administracion/menu/guardarMenuPrincipal')?>" method="POST" id="nuevo_menu">
                    <?php echo form_hidden($this->security->get_csrf_token_name(), $this->security->get_csrf_hash()); ?>
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="nombre">Men&uacute;</label>
                                <input class="span5" id="nombre" name="nombre" type="text" placeholder="Nombre del nuevo menu"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">

                                <label class="form-label span4" for="es_padre">&iquest;Tiene Sub-Menus?</label>
                                
                                <div class="span8 controls">
                                   
                                    <div class="left marginR10">
                                        <input type="checkbox" id="es_padre" name="es_padre" class="ibuttonCheck nostyle" />
                                    </div>

                                </div>
                                
                            </div>
                        </div> 
                    </div>
                    <br>
                    <div class="row-fluid">
                        <div class="span2"></div>
                        <div class="span9">

                            <div class="box gradient">

                                <div class="title">
                                    <h4>
                                        <span>Seleccione un Icono de Menu</span>
                                    </h4>
                                </div>
                                <div class="content">
                                    <div class="scroll" style="height: 200px; overflow-y: hidden; overflow-x: auto; margin-top: 10px; outline: none;" tabindex="5000">                                   
                                    <?php foreach ( $listado_iconos->result() as $icono ) : ?>
                                        <span class="box1 tip iconos" title="Doble click para seleccionar" data-icon="<?=$icono->ICO_NOMBRE?>">
                                            <span aria-hidden="true" class="<?=$icono->ICO_NOMBRE?>"></span> 
                                            <?=$icono->ICO_NOMBRE?>
                                        </span>
                                    <?php endforeach; ?>
                                    </div>
                                </div>

                            </div><!-- End .box -->

                        </div><!-- End .span6 -->

                    </div>
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="opcion">Icono Seleccionado</label>
                                <span class="box1" id="selected_icon">
                                    <span aria-hidden="true" class="" id="display_icon"></span>&nbsp;
                                </span>
                                <input type="hidden" name="icono" id="hidden_icono" value="">
                            </div>
                        </div>
                    </div>
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="prependedInput">URL</label>
                                <div class="input-prepend">
                                    <span class="add-on"><?=base_url()?></span><input class="span12 tip" type="text"  id="url" name="url" title="URL de la p&aacute;gina" placeholder="controller/function"/>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span4" for="descripcion">Descripci&oacute;n</label>
                                <textarea class="span8 limit" id="descripcion" name="descripcion" rows="3" placeholder="Breve descripci&oacute;n para el tooltip"></textarea>
                            </div>
                        </div>
                    </div>

                    
                    
                    <div class="form-actions">
                       <button type="submit" class="btn btn-success">Guardar</button>
                       <button type="reset" class="btn btn-danger">Cancelar</button>
                    </div>
                                                            

                </form>
             
            </div>

        </div><!-- End .box -->

    </div><!-- End .span12 -->

</div><!-- End .row-fluid -->