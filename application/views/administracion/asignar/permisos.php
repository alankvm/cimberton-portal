<div class="row-fluid">

    <div class="span12">

        <div class="box">
            <?php
            //Construyendo datos para el feedback 
            $parametros = array(
                'segmento' => 4,
                'exito'    => 'Los permisos de navegaci&oacuten han sido asignados exitosamente',
                'error'    => 'Tuvimos un inconveniente, por favor intentalo de nuevo'
                );
            //Imprimiendo alerta resultante de la operacion
            print $this->model_alertas->setAlerta( $parametros );
                
            ?>
            <div class="title">

                <h4> 
                    <span>Asignaci&oacute;n de Permisos</span>
                </h4>
                
            </div>
            <div class="content">
               
                <?=form_open('administracion/asignar/opciones')?>
                    
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <label class="form-label span3" for="checkboxes">Cargos</label>
                                <div class="span4 controls">   
                                    <select name="cargos" id="select1" class="nostyle" style="width:100%;" placeholder="Por favor, seleccione un cargo">
                                           <option></option>
                            <?php if ( $listado_cargos ) :
                                        foreach ( $listado_cargos->result() as $cargo ) :?>
                                        <option value="<?=$cargo->CAR_ID?>"><?=$cargo->CAR_NOMBRE?></option>
                            <?php       endforeach; 
                                  endif; ?>
                                      </select>
                                </div> 

                            </div>
                        </div> 
                    </div>
                    
                    <div class="form-actions">
                       <button type="submit" class="btn btn-primary">Ver opciones de navegaci&oacute;n</button>
                       <button type="reset" class="btn">Cancelar</button>
                    </div>
                                                            

                <?=form_close()?>
             
            </div>

        </div><!-- End .box -->

    </div><!-- End .span12 -->

</div><!-- End .row-fluid -->
