<div class="row-fluid">

    <div class="span12">

        <div class="box">
            
            <div class="title">

                <h4> 
                    <span>Asignaci&oacute;n de Permisos</span>
                </h4>
                
            </div>
            <div class="content">
               
                <?=form_open('administracion/asignar/establecer')?>
                <input type="hidden" name="id_cargo" value="<?=$id_cargo?>">
                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">
                                <div class="span9" style="margin-left: 9%;">
                                    <div class="page-header center">
                                        <h4>Opciones de Men&uacute;</h4>
                                    </div>
                                    <?=$listado_menus?>
                                </div>
                            </div>
                        </div> 
                    </div>
                    
                    <div class="form-actions">
                       <button type="submit" class="btn btn-primary">Guardar</button>
                       <a href="<?=base_url('administracion/asignar/permisos')?>" class="btn btn-danger">Regresar</a>
                    </div>
                                                            

                <?=form_close()?>
             
            </div>

        </div><!-- End .box -->

    </div><!-- End .span12 -->

</div><!-- End .row-fluid -->