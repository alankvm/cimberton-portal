<!-- Esta seccion de filtro es Generada desde el archivo tablero/gerencia/filtro.php -->
<div class="row-fluid">
    <div class="span12">
         
        <div class="box closed">
            <div class="title">

                <h4>
                    <span class="icomoon-icon-vector"></span>
                    <span>Cambiar Periodo Activo</span>
                </h4>
                <a href="#" class="minimize" style="display: none;">Minimize</a>
            </div>
            <div class="content">
                <?php echo form_open('tablero/gerencia/filtrar', array('id' => 'frm-filtro')); ?>
                    <div class="form-row row-fluid" >
                        <div class="span6" id="filtro-mes">
                            <div class="row-fluid">
                                <label class="form-label span6" for="mes">Mes</label>
                                <div class="span6 controls">   
                                    <select name="mes" id="mes"  class="nostyle" style="width:100%;" placeholder="Selecciona un mes">
                                           <option></option>
                                        <option value="1">Enero</option>
                                        <option value="2">Febrero</option>
                                        <option value="3">Marzo</option>
                                        <option value="4">Abril</option>
                                        <option value="5">Mayo</option>
                                        <option value="6">Junio</option>
                                        <option value="7">Julio</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Septiembre</option>
                                        <option value="10">Octubre</option>
                                        <option value="11">Noviembre</option>
                                        <option value="12">Diciembre</option>
                                      </select>
                                </div> 
                            </div>
                        </div> 
                    
                        <div class="span6">
                            <div class="row-fluid">
                                <label class="form-label span2" for="year">A&ntilde;o</label>
                                <div class="span5 controls">   
                                    <select name="year" id="year"  class="nostyle" style="width:100%;" placeholder="Selecciona un a&ntilde;o">
                                           <option></option>
                                <?php foreach ( $listado_years->result() as $fila ): ?>
                                        <option value="<?=$fila->ANO_PERIODO?>"><?=$fila->ANO_PERIODO?></option>
                                <?php endforeach; ?>
                                      </select>
                                </div> 
                            </div>
                        </div> 
                    </div>

                    <div class="form-row row-fluid">
                        <div class="span12">
                            <div class="row-fluid">

                                <label class="form-label span3" for="acumulado">&iquest;Mostrar Acumulado?</label>
                                
                                <div class="span8 controls">
                                    <input type="checkbox" id="acumulado" name="acumulado" class="ibutton1 nostyle " /> 
                                </div>
                                
                            </div>
                        </div> 
                    </div>
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-info">Cambiar</button>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<!-- fin seccion filtro -->