<div class="row-fluid">
    
    <div class="span12">

        <div class="box">

            <div class="title">

                <h4>
                    <span class=" icomoon-icon-chart"></span>
                    <span><?=$periodo?>Resumen de Ventas</span>
                </h4>
                <a href="#" class="minimize" style="display: none;">Minimize</a>
            </div>
            <div class="content noPad">
                <table class="table table-condensed" id="ventas">
                  <thead>
                  <tr>
                      <th>Distribuci&oacute;n</th>
                      <th>&nbsp;</th>
                      <th>&nbsp;</th>
                      <th>Ventas</th>
                      <th>&nbsp;</th>
                      <th>&nbsp;</th>
                      <th >&nbsp;</th>
                      <th>Cobros</th>
                      <th>&nbsp;</th>
                    </tr>
                    <tr>
                      <th>Canal</th>
                      <th>A&ntilde;o Actual</th>
                      <th>Presupuesto</th>
                      <th>Cumplimiento</th>
                      <th>A&ntilde;o Anterior</th>
                      <th>Cumplimiento</th>
                       <th>A&ntilde;o Actual</th>
                      <th>Presupuesto</th>
                      <th>Cumplimiento</th>
                    </tr>
                  </thead>
                  <tbody>
            <?php if ( $resumen_gerencia ):
                  foreach ( $resumen_gerencia->result() as $fila ) : ?>
                    <tr>
                      <td ><?=$fila->DESCRIPCION_CANAL?></td>
                      <td><?= number_format( $fila->ACTUAL, 0 ) ?></td>
                      <td><?= number_format( $fila->PRONOSTICO, 0 ) ?></td>
                      <td <?php calcular_colores($fila->CUMPLIMIENTO1);?> ><?= $fila->CUMPLIMIENTO1 * 100 ?>%</td>
                      <td><?= number_format( $fila->ANTERIOR, 0 ) ?></td>
                      <td <?php calcular_colores($fila->CUMPLIMIENTO2)?>><?=$fila->CUMPLIMIENTO2 * 100?>%</td>
                      <td><?= number_format( $fila->COBRO, 0 ) ?></td>
                      <td><?= number_format( $fila->COBRO_PRONOSTICO, 0 ) ?></td>
                      <td <?php calcular_colores($fila->CUMPLIMIENTO3)?> ><?=$fila->CUMPLIMIENTO3 * 100?>%</td>
                    </tr>
            <?php endforeach; 
                  else:?>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>0 Datos Encontrados</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
            <?php endif; ?>
                  </tbody>
                </table>
            </div>

        </div><!-- End .box -->

    </div><!-- End .span12 -->

</div>
