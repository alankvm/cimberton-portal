<div class="row-fluid">
    <div class="span12">

        <div class="box">

            <div class="title">

                <h4>
                    <span class="icon16 icomoon-icon-equalizer-2"></span>
                    <span><?=$periodo?> | Ventas Canal Farmacia - Privados</span>
                </h4>
                <a href="#" class="minimize" style="display: none;">Minimize</a>
            </div>
            <div class="content noPad">
                <table class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th>C&oacute;digo</th>
                            <th>Marca</th>
                            <th>A&ntilde;o Actual</th>
                            <th>Presupuesto</th>
                            <th>Cumplimiento</th>
                            <th>A&ntilde;o Anterior</th>
                            <th>Cumplimiento</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ( $listado ):
                        foreach ( $listado->result() as $fila ): 
                            if ( $fila->ACTUAL > 2000 ): ?>
                        <tr>
                            <td><?=$fila->CODIGO_MARCA?></td>
                            <td><?=tiene_modal( $fila->DESCRIPCION_MARCA )?></td>
                            <td><?=number_format( $fila->ACTUAL, 0 )?></td>
                            <td><?=number_format($fila->PRONOSTICO, 0 )?></td>
                            <td <?=color($fila->CUMPLIMIENTO1)?> ><?=number_format($fila->CUMPLIMIENTO1, 0 )?>%</td>
                            <td><?=number_format($fila->ANTERIOR, 0 )?></td>
                            <td <?=color($fila->CUMPLIMIENTO2)?> ><?=number_format($fila->CUMPLIMIENTO2, 0 )?>%</td>
                        </tr>
                    <?php   endif;
                    endforeach; recalcularTotales($total, $otros)?>
                        <tr>
                            <td>---</td>
                            <td><a href="#" id="otras-marcas">OTRAS MARCAS</a></td>
                            <td><?=number_format( $otros->ACTUAL, 0 )?></td>
                            <td><?=number_format($otros->PRONOSTICO, 0 )?></td>
                            <td <?=color(( $otros->ACTUAL/$otros->PRONOSTICO ) * 100)?> ><?=number_format( ( $otros->ACTUAL/$otros->PRONOSTICO ) * 100, 0 )?>%</td>
                            <td><?=number_format($otros->ANTERIOR, 0 )?></td>
                            <td <?=color(( $otros->ACTUAL/$otros->ANTERIOR ) * 100)?> ><?=number_format( ( $otros->ACTUAL/$otros->ANTERIOR ) * 100, 0 )?>%</td>
                        </tr>
                        <tr>
                            <td><strong>GRAN TOTAL</strong></td>
                            <td></td>
                            <td><?=number_format( $total->ACTUAL, 0 )?></td>
                            <td><?=number_format($total->PRONOSTICO, 0 )?></td>
                            <td <?=color(( $total->ACTUAL/$total->PRONOSTICO ) * 100)?> ><?=number_format( ( $total->ACTUAL/$total->PRONOSTICO ) * 100, 0 )?>%</td>
                            <td><?=number_format($total->ANTERIOR, 0 )?></td>
                            <td <?=color(( $total->ACTUAL/$total->ANTERIOR ) * 100)?> ><?=number_format( ( $total->ACTUAL/$total->ANTERIOR ) * 100, 0 )?>%</td>
                        </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div><!-- End .box -->
    </div>
</div>


<!-- ===============================     Empiezan los modales  ===================================================-->
<div id="marcas" class="modal hide fade marcas-large" style="display: none; ">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
        <h3>Otras Marcas: <?=$periodo?> | Ventas Canal Farmacia - Privados</h3>
    </div>
    <div class="modal-body">
        <div class="paddingT5 paddingB5">    
             <table class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>C&oacute;digo</th>
                        <th>Marca</th>
                        <th>A&ntilde;o Actual</th>
                        <th>Presupuesto</th>
                        <th>Cumplimiento</th>
                        <th>A&ntilde;o Anterior</th>
                        <th>Cumplimiento</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ( $listado ):
                    foreach ( $listado->result() as $fila ): 
                        if ( $fila->ACTUAL < 2000 ): ?>
                    <tr>
                        <td><?=$fila->CODIGO_MARCA?></td>
                        <td><?=$fila->DESCRIPCION_MARCA?></td>
                        <td><?=number_format( $fila->ACTUAL, 0 )?></td>
                        <td><?=number_format($fila->PRONOSTICO, 0 )?></td>
                        <td <?=color($fila->CUMPLIMIENTO1)?> ><?=number_format($fila->CUMPLIMIENTO1, 0 )?>%</td>
                        <td><?=number_format($fila->ANTERIOR, 0 )?></td>
                        <td <?=color($fila->CUMPLIMIENTO2)?> ><?=number_format($fila->CUMPLIMIENTO2, 0 )?>%</td>
                    </tr>
                <?php   endif;
                endforeach;?>
                    <tr>
                        <td><strong>GRAN TOTAL</strong></td>
                        <td></td>
                        <td><?=number_format( $otros->ACTUAL, 0 )?></td>
                        <td><?=number_format($otros->PRONOSTICO, 0 )?></td>
                        <td <?=color(( $otros->ACTUAL/$otros->PRONOSTICO ) * 100)?> ><?=number_format( ( $otros->ACTUAL/$otros->PRONOSTICO ) * 100, 0 )?>%</td>
                        <td><?=number_format($otros->ANTERIOR, 0 )?></td>
                        <td <?=color(( $otros->ACTUAL/$otros->ANTERIOR ) * 100)?> ><?=number_format( ( $otros->ACTUAL/$otros->ANTERIOR ) * 100, 0 )?>%</td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>

    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Cerrar</a>
    </div>
</div>

<div id="pyg-marcas" class="modal hide fade marcas-large" style="display: none; ">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span class="icon12 minia-icon-close"></span></button>
        <h3>PROCTER &amp; GAMBLE FARMACIA: <?=$periodo?> | Ventas Canal Farmacia - Privados</h3>
    </div>
    <div class="modal-body">
        <div class="paddingT5 paddingB5">    
             <table class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>C&oacute;digo</th>
                        <th>Marca</th>
                        <th>A&ntilde;o Actual</th>
                        <th>Presupuesto</th>
                        <th>Cumplimiento</th>
                        <th>A&ntilde;o Anterior</th>
                        <th>Cumplimiento</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ( $pyg ):
                    foreach ( $pyg->result() as $fila ): ?>
                    <tr>
                        <td><?=$fila->CODIGO_MARCA?></td>
                        <td><?=$fila->DESCRIPCION_MARCA?></td>
                        <td><?=number_format( $fila->ACTUAL, 0 )?></td>
                        <td><?=number_format($fila->PRONOSTICO, 0 )?></td>
                        <td <?=color($fila->CUMPLIMIENTO1)?> ><?=number_format($fila->CUMPLIMIENTO1, 0 )?>%</td>
                        <td><?=number_format($fila->ANTERIOR, 0 )?></td>
                        <td <?=color($fila->CUMPLIMIENTO2)?> ><?=number_format($fila->CUMPLIMIENTO2, 0 )?>%</td>
                    </tr>
                <?php endforeach;?>
                    <tr>
                        <td><strong>GRAN TOTAL</strong></td>
                        <td></td>
                        <td><?=number_format( $totalpyg->ACTUAL, 0 )?></td>
                        <td><?=number_format($totalpyg->PRONOSTICO, 0 )?></td>
                        <td <?=color(( $totalpyg->ACTUAL/$totalpyg->PRONOSTICO ) * 100)?> ><?=number_format( ( $totalpyg->ACTUAL/$totalpyg->PRONOSTICO ) * 100, 0 )?>%</td>
                        <td><?=number_format($totalpyg->ANTERIOR, 0 )?></td>
                        <td <?=color(( $totalpyg->ACTUAL/$totalpyg->ANTERIOR ) * 100)?> ><?=number_format( ( $totalpyg->ACTUAL/$totalpyg->ANTERIOR ) * 100, 0 )?>%</td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>

    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Cerrar</a>
    </div>
</div>