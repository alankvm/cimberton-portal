<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="title">
                <h4><?=$periodo?> | Ventas Canal Farmacia</h4>
            </div>
            <div class="content">

                <ul class="unstyled">
                    <li>
                        <span class="icon16 icomoon-icon-arrow-right-2"></span>
                        <strong>D&iacute;as objetivos del mes: <span class="blue"> <?=$dias_objetivos?> </span> 
                        D&iacute;as efectivos del mes: <span class="blue"> <?=$dias_efectivos?> </span></strong>
                    </li>
                    <li>
                        <span class="icon16 icomoon-icon-arrow-right-2"></span>
                        <strong>Total objetivo de venta del mes: <span class="blue"> <?=number_format( $pronostico, 0 )?> </span> 
                        - Porcentaje alcanzado: <span class="blue"> <?=number_format( ( $total_distri->ACTUAL/$pronostico ) * 100, 0)?> %</span></strong>
                    </li>
                      <li>
                        <span class="icon16 icomoon-icon-arrow-right-2"></span>
                        <strong>Total objetivo de cobro del mes: <span class="blue"> <?=number_format( $cobro, 0 )?> </span> 
                        - Porcentaje alcanzado: <span class="blue"> <?=number_format(( $total_distri->COBRO/$cobro ) * 100, 0 )?> %</span></strong>
                    </li>
                    
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="content">
            <div class="span12">

                <div class="box">

                    <div class="title">

                        <h4>
                            <span class="icon16 icomoon-icon-equalizer-2"></span>
                            <span>Ventas Por Canal De Distribuci&oacute;n</span>
                        </h4>
                        <a href="#" class="minimize" style="display: none;">Minimize</a>
                    </div>
                    <div class="content noPad">
                        <table class="table table-condensed">
                          <thead>
                            <tr>
                              <th>Descripci&oacute;n</th>
                              <th></th>
                              <th></th>
                              <th>Ventas</th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th>Cobros</th>
                              <th></th>
                            </tr>
                            <tr>
                              <th>Canal</th>
                              <th>A&ntilde;o Actual</th>
                              <th>Presupuesto</th>
                              <th>Cumplimiento</th>
                              <th>A&ntilde;o Anterior</th>
                              <th>Cumplimiento</th>
                              <th>Cobro</th>
                              <th>Presupuesto</th>
                              <th>Cumplimiento</th>
                            </tr>
                          </thead>
                          <tbody>
                <?php 
                if ( $distribucion ):
                   
                    foreach ( $distribucion->result() as $fila ): ?>
                            <tr>
                                <td><?=$fila->DESCRIPCION_CANAL?></td>
                                <td><?=number_format( $fila->ACTUAL, 0 )?></td>
                                <td><?=number_format( $fila->PRONOSTICO, 0)?></td>
                                <td <?=color($fila->CUMPLIMIENTO1)?>><?=number_format( $fila->CUMPLIMIENTO1, 0 )?>%</td>
                                <td ><?=number_format( $fila->ANTERIOR,0 )?></td>
                                <td <?=color($fila->CUMPLIMIENTO2)?>><?=number_format( $fila->CUMPLIMIENTO2, 0 )?>%</td>
                                <td><?=number_format( $fila->COBRO, 0 )?></td>
                                <td><?=number_format( $fila->COBRO_PRONOSTICO, 0 )?></td>
                                <td <?=color($fila->CUMPLIMIENTO3)?>><?=number_format( $fila->CUMPLIMIENTO3, 0 )?>%</td>     
                            </tr>
                <?php endforeach; ?>
                             <tr>
                                <td><strong>GRAN TOTAL</strong></td>
                                <td><?=number_format($total_distri->ACTUAL, 0)?></td>
                                <td><?=number_format($total_distri->PRONOSTICO, 0)?></td>
                                <td <?=color(( $total_distri->ACTUAL/$total_distri->PRONOSTICO)*100)?>><?=number_format(( $total_distri->ACTUAL/$total_distri->PRONOSTICO)*100, 0)?>%</td>
                                <td><?=number_format($total_distri->ANTERIOR, 0)?></td>
                                <td <?=color(($total_distri->ACTUAL/$total_distri->ANTERIOR)*100)?>><?=number_format(($total_distri->ACTUAL/$total_distri->ANTERIOR)*100, 0)?>%</td>
                                <td><?=number_format($total_distri->COBRO, 0)?></td>
                                <td><?=number_format($total_distri->COBRO_PRONOSTICO, 0)?></td>
                                <td <?=color(($total_distri->COBRO/$total_distri->COBRO_PRONOSTICO)*100)?>><?=number_format(($total_distri->COBRO/$total_distri->COBRO_PRONOSTICO)*100, 0)?>%</td>
                            </tr>
                <?php endif;?>
                          </tbody>
                        </table>
                    </div>

                </div><!-- End .box -->

            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">

        <div class="box">

            <div class="title">

                <h4>
                    <span class="icon16 icomoon-icon-equalizer-2"></span>
                    <span>Ventas Por Regi&oacute;n - Canal Privado</span>
                </h4>
                <a href="#" class="minimize" style="display: none;">Minimize</a>
            </div>
            <div class="content noPad">
                <table class="table table-condensed">
                  <thead>
                    <tr>
                      <th>Descripci&oacute;n</th>
                      <th></th>
                      <th></th>
                      <th>Ventas</th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th>Cobros</th>
                      <th></th>
                    </tr>
                    <tr>
                      <th>Canal</th>
                      <th>A&ntilde;o Actual</th>
                      <th>Presupuesto</th>
                      <th>Cumplimiento</th>
                      <th>A&ntilde;o Anterior</th>
                      <th>Cumplimiento</th>
                      <th>Cobro</th>
                      <th>Presupuesto</th>
                      <th>Cumplimiento</th>
                    </tr>
                  </thead>
                  <tbody>
        <?php if ( $regional ):
                foreach ( $regional->result() as $fila ):?>
                     <tr>
                        <td><?=ucwords( $fila->DESCRIPCION_CANAL )?></td>
                        <td><?=number_format( $fila->ACTUAL, 0 )?></td>
                        <td><?=number_format( $fila->PRONOSTICO, 0)?></td>
                        <td <?=color($fila->CUMPLIMIENTO1)?>><?=number_format( $fila->CUMPLIMIENTO1, 0)?>%</td>
                        <td ><?=number_format( $fila->ANTERIOR,0 )?></td>
                        <td <?=color($fila->CUMPLIMIENTO2)?>><?=number_format( $fila->CUMPLIMIENTO2, 0 )?>%</td>
                        <td><?=number_format( $fila->COBRO, 0 )?></td>
                        <td><?=number_format( $fila->COBRO_PRONOSTICO, 0 )?></td>
                        <td <?=color($fila->CUMPLIMIENTO3)?>><?=number_format( $fila->CUMPLIMIENTO3, 0 )?>%</td>     
                    </tr>
        <?php endforeach; ?>
                     <tr>
                        <td><strong>GRAN TOTAL</strong></td>
                        <td><?=number_format( $total_region->ACTUAL, 0)?></td>
                        <td><?=number_format( $total_region->PRONOSTICO, 0)?></td>
                        <td <?=color(($total_region->ACTUAL/$total_region->PRONOSTICO)*100)?>><?=number_format( ($total_region->ACTUAL/$total_region->PRONOSTICO)*100, 0)?>%</td>
                        <td><?=number_format( $total_region->ANTERIOR, 0)?></td>
                        <td <?=color(($total_region->ACTUAL/$total_region->ANTERIOR)*100)?>><?=number_format( ($total_region->ACTUAL/$total_region->ANTERIOR)*100, 0)?>%</td>
                        <td><?=number_format( $total_region->COBRO, 0)?></td>
                        <td><?=number_format( $total_region->COBRO_PRONOSTICO, 0)?></td>
                        <td <?=color(($total_region->COBRO/$total_region->COBRO_PRONOSTICO)*100)?>><?=number_format( ($total_region->COBRO/$total_region->COBRO_PRONOSTICO)*100, 0)?>%</td>
                    </tr>
        <?php endif;?>
                  </tbody>
                </table>
            </div>

        </div><!-- End .box -->

    </div>
</div>