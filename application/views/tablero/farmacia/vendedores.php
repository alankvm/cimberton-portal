<div class="row-fluid">
    <div class="span12">

        <div class="box">

            <div class="title">

                <h4>
                    <span class="icon16 icomoon-icon-equalizer-2"></span>
                    <span><?=$periodo?> | Ventas Canal Farmacia - Vendedores</span>
                </h4>
                <a href="#" class="minimize" style="display: none;">Minimize</a>
            </div>
            <div class="content noPad">
                <table class="table table-condensed">
                  <thead>
                    <tr>
                      <th>Descripci&oacute;n</th>
                      <th></th>
                      <th></th>
                      <th>Ventas</th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th>Cobros</th>
                      <th></th>
                      <th></th>
                    </tr>
                    <tr>
                      <th>Nombre</th>
                      <th>A&ntilde;o Actual</th>
                      <th>Presupuesto</th>
                      <th>Cumplimiento</th>
                      <th>A&ntilde;o Anterior</th>
                      <th>Cumplimiento</th>
                      <th>Cobro</th>
                      <th>Presupuesto</th>
                      <th>Cumplimiento</th>
                      <th>Ver</th>
                    </tr>
                  </thead>
                  <tbody>
        <?php   if ( $listado ): 
                    foreach ( $listado->result() as $fila ): ?>
                    <tr>
                      <td><?=clearName( $fila->NOMBRE_VENDEDOR )?></td>
                      <td><?=number_format($fila->ACTUAL, 0 )?></td>
                      <td><?=number_format($fila->PRONOSTICO, 0 )?></td>
                      <td <?=color($fila->CUMPLIMIENTO1)?>><?=number_format($fila->CUMPLIMIENTO1, 0 )?>%</td>
                      <td><?=number_format($fila->ANTERIOR, 0 )?></td>
                      <td <?=color($fila->CUMPLIMIENTO2)?>><?=number_format($fila->CUMPLIMIENTO2, 0 )?>%</td>
                      <td><?=number_format($fila->COBRO, 0 )?></td>
                      <td><?=number_format($fila->COBRO_PRONOSTICO, 0 )?></td>
                      <td <?=color($fila->CUMPLIMIENTO3)?>><?=number_format($fila->CUMPLIMIENTO3, 0 )?>%</td>
                      <td>
                        <div class="controls center">
                            <a href="#" class="tip" oldtitle="Edit task" title="Ver Clientes"><span class="icomoon-icon-users"></span></a>
                            <a href="#" class="tip" oldtitle="Remove task" title="Ver Productos"><span class="icomoon-icon-box"></span></a>
                        </div>
                      </td>
                    </tr>
        <?php       endforeach; ?>
                    <tr>
                      <td>GRAN TOTAL</td>
                      <td><?=number_format($total->ACTUAL, 0 )?></td>
                      <td><?=number_format($total->PRONOSTICO, 0 )?></td>
                      <td <?=color( ($total->ACTUAL/$total->PRONOSTICO)*100 )?>><?=number_format( ($total->ACTUAL/$total->PRONOSTICO)*100, 0 )?>%</td>
                      <td><?=number_format($total->ANTERIOR, 0 )?></td>
                      <td <?=color(($total->ACTUAL/$total->ANTERIOR)*100)?>><?=number_format(($total->ACTUAL/$total->ANTERIOR)*100, 0 )?>%</td>
                      <td><?=number_format($total->COBRO, 0 )?></td>
                      <td><?=number_format($total->COBRO_PRONOSTICO, 0 )?></td>
                      <td <?=color(($total->COBRO/$total->COBRO_PRONOSTICO)*100)?>><?=number_format(($total->COBRO/$total->COBRO_PRONOSTICO)*100, 0 )?>%</td>
                      <td>
                        <div class="controls center">
                            <a href="#" class="tip" oldtitle="Edit task" title="Ver Clientes"><span class="icomoon-icon-users"></span></a>
                            <a href="#" class="tip" oldtitle="Remove task" title="Ver Productos"><span class="icomoon-icon-box"></span></a>
                        </div>
                      </td>
                    </tr>
            <?php endif; ?>
                  </tbody>
                </table>
            </div>

        </div><!-- End .box -->

    </div>
</div>