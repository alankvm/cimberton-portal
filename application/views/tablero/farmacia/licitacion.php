<div class="row-fluid">
    <div class="span12">

        <div class="box">

            <div class="title">

                <h4>
                    <span class="icon16 icomoon-icon-equalizer-2"></span>
                    <span><?=$periodo?> | Ventas Canal Farmacia - Licitaciones</span>
                </h4>
                <a href="#" class="minimize" style="display: none;">Minimize</a>
            </div>
            <div class="content noPad">
                <table class="table table-condensed">
                  <thead>
                    <tr>
                      <th>C&oacute;digo</th>
                      <th>Marca</th>
                      <th>A&ntilde;o Actual</th>
                      <th>A&ntilde;o Anterior</th>
                      <th>Cumplimiento</th>
                    </tr>
                  </thead>
                  <tbody>
        <?php   if ( $listado ): 
                    foreach ( $listado->result() as $fila ): ?>
                    <tr>
                      <td><?=$fila->CODIGO_MARCA?></td>
                      <td><?=$fila->DESCRIPCION_MARCA?></td>
                      <td><?=number_format( $fila->ACTUAL, 0 )?></td>
                      <td><?=number_format( $fila->ANTERIOR, 0 )?></td>
                      <td <?=color($fila->CUMPLIMIENTO1)?>><?=number_format( $fila->CUMPLIMIENTO1, 0 )?></td>
                    </tr>
        <?php       endforeach;
                endif; ?>
                  </tbody>
                </table>
            </div>

        </div><!-- End .box -->

    </div>
</div>