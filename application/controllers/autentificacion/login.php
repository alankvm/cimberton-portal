<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('autentificacion');
        $this->load->helper('url');
        $this->load->model('model_alertas');
    }
    /**
     * Formulario de accesso
     * @return object login form
     */
    public function index()
    {
        
        $this->load->view('autentificacion/login');
    }

    public function validar()
    {
        //Inicializar variables
        $usuario = $this->input->post('username');
        $password = $this->input->post('password');
        $this->autentificacion->verificarUsuario( $usuario, $password );
        if ( !$this->autentificacion->isLoggedIn() ) {
            $this->session->set_flashdata( 'error', 'Usuario o Contrase&ntilde;a incorrecto.' );
            redirect();

        } else {
            
            $es_primer_login = $this->autentificacion->esPrimerLogin( $usuario );

            if ( !$es_primer_login ) {
                redirect('panel/control');    
            } else {
                redirect( 'autentificacion/perfil' ); 
            }
            
        }
    }

    /*
        Validacion de usuario Sybase
     */
    public function validarUsuarioSybase()
    {
        if ( !$this->input->is_ajax_request() ) {
           exit('No direct script access allowed');
        }
        
        $usuario = $this->input->get('usuario');
        $resultado = $this->autentificacion->comprobarUsuarioSybase( $usuario );

        echo json_encode( $resultado );
    }

    public function cerrar()
    {
        $this->autentificacion->logOut();
        redirect();
    }
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */