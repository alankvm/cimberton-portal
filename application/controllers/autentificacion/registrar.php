<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class registrar extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_autentificacion');
    }

    public function index()
    {
        try{
            //Instanciando grocery crud
            $crud = new grocery_CRUD();
            //Columnas a mostrar
            $columns = array( 'USU_NOMBRE', 'USU_APELLIDO', 'USU_USUARIO', 'USU_ID_CAR' );
            $fields = array( 'USU_NOMBRE', 'USU_APELLIDO', 'USU_USUARIO', 'USU_ID_CAR' );
            //Configuracion necesaria para el registro de usuarios
            $crud->set_table( 'cimberton_usu_usuario' )
            ->set_subject( 'Usuario' )
            ->set_relation( 'USU_ID_CAR', 'cimberton_car_cargo', 'CAR_NOMBRE')
            ->columns( $columns )
            ->display_as( 'USU_NOMBRE', 'Nombres' )
            ->display_as( 'USU_APELLIDO', 'Apellidos' )
            ->display_as( 'USU_USUARIO', 'Usuario' )
            ->display_as( 'USU_ID_CAR', 'Cargo' )
            ->set_rules( 'USU_NOMBRE', 'Nombres', 'required|min_length[3]' )
            ->set_rules( 'USU_APELLIDO', 'Apellidos', 'required|min_length[3]' )
            ->set_rules( 'USU_USUARIO', 'Usuario', 'required|is_unique[cimberton_usu_usuario.USU_USUARIO]' )
            ->set_rules( 'USU_ID_CAR', 'Cargo', 'required' )
            ->fields( $fields )
            ->callback_after_insert( array( $this, 'setPassword' ) );
            //Generando Crud
            $output = $crud->render();
            if ( !$this->autentificacion->isLoggedIn() ) {
                redirect();
            } else { 
                //Cargalo Crud en Pantalla
                $this->_mostrar_crud( $output );
            }
            

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
        
    }

    public function cargos()
    {
        try{
            //Instanciando grocery crud
            $crud = new grocery_CRUD();
            //Columnas a mostrar
            $columns = array( 'CAR_ID', 'CAR_NOMBRE', 'CAR_DESCRIPCION' );
            $fields = array( 'CAR_NOMBRE', 'CAR_DESCRIPCION' );
            //Configuracion necesaria para el registro de usuarios
            $crud->set_table( 'cimberton_car_cargo' )
            ->set_subject( 'Cargos' )
            ->columns( $columns )
            ->display_as( 'CAR_ID', 'C&oacute;digo' )
            ->display_as( 'CAR_NOMBRE', 'Nombre del Cargo' )
            ->display_as( 'CAR_DESCRIPCION', 'Descripci&oacute;n' )
            ->set_rules( 'CAR_NOMBRE', 'Cargo', 'required|min_length[3]' )
            ->fields( $fields );
            //Generando Crud
            $output = $crud->render();
            if ( !$this->autentificacion->isLoggedIn() ) {
                redirect();
            } else { 
                //Cargalo Crud en Pantalla
                $this->_mostrar_crud( $output );
            }
            

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
        
    }
    
    /**
     * Establece password al registrar un nuevo usuario en el sistema 
     * @param array $post_array datos recien ingresados en la base de datos
     * @param integer $primary_key llave primaria del registro recien almacenado
     */
    public function setPassword( $post_array, $primary_key )
    {
        return $this->model_autentificacion->establecerPasswordInicial( $primary_key, $post_array );
    }

    /**
     * Cargar vista de tipo CRUD
     * @param  object $output resultado de la generacion del crud
     */
    public function _mostrar_crud($output = null)
    {
        $this->masterpage->cargarCrud( $output );
    }



}

/* End of file registrar.php */
/* Location: ./application/controllers/registrar.php */