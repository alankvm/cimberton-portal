<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('autentificacion');
        $this->load->model('model_autentificacion');
    }

    public function index()
    {
        $labels = array(
            array( 'titulo' => 'Autentificaci&oacuteln - Perfil' ),
            array( 'ubicacion' => 'Perfil' )
            );
        $js = array(
            'js/cimberton/administracion/autentificacion/perfil.js'
            );
        
        $elementos = array(
            'form',
            'widget'
            );

        $view = 'perfil/index.php';

        $data['informacion'] = $this->autentificacion->getInformacionUsuarioActivo();

        if ( !$this->autentificacion->isLoggedIn() ) { 
            redirect();
        } else {
            $this->masterpage->cargar( $view, $labels, $data, $elementos, $js );
        }
    }

    public function actualizar()
    {
        $resultado = $this->model_autentificacion->actualizarPassword( $this->input->post() );
        redirect( 'autentificacion/perfil/index/'. $resultado, 'location' );
    }

}

/* End of file perfil.php */
/* Location: ./application/controllers/perfil.php */