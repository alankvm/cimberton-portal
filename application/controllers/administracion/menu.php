<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_administracion');
        $this->load->model('model_alertas');
        $this->load->model('model_navegacion');
    }

    public function index()
    {
        $this->masterpage->cargar();
    }

    public function nuevo()
    {
        $labels = array(
            array( 'titulo' => 'Administraci&oacute;n - Menus' ),
            array( 'ubicacion' => 'Nuevo Men&uacute' )
            );
        $js = array(
            'js/cimberton/administracion/menu/nuevo.js'
            );
        $elementos = array(
            'form',
            'widget'
            );
        $view = 'administracion/menu/nuevo.php';
        $data['listado_iconos'] = $this->model_navegacion->getListadoIconosDisponibles();
        if ( !$this->autentificacion->isLoggedIn() ) { 
            redirect();
        } else {
            $this->masterpage->cargar( $view, $labels, $data, $elementos, $js );
        }
    }

    public function submenu( $id_menu_padre = 0 )
    {
        $labels = array(
            array( 'titulo' => 'Administraci&oacute;n - Menus' ),
            array( 'ubicacion' => 'Nueva Opci&oacute;n' )
            );
        $js = array(
            'js/cimberton/administracion/menu/submenu.js'
            );
        $elementos = array(
            'form',
            'widget'
            );
        $view = 'administracion/menu/submenu.php';
        $data['listado_menus_principales'] = $this->model_navegacion->getListadoMenusPrincipales();
        $data['listado_iconos'] = $this->model_navegacion->getListadoIconosDisponibles();
        if ( !$this->autentificacion->isLoggedIn() ) { 
            redirect();
        } else {
            $this->masterpage->cargar( $view, $labels, $data, $elementos, $js );
        }
    }

    public function guardarMenuPrincipal()
    {
        //Guarda nuevo menu principal
        $resultado = $this->model_navegacion->NuevoMenuPrincipal();
        redirect( base_url('administracion/menu/nuevo/'. $resultado, 'location') );
    }

    public function guardarSubMenu()
    {
        $resultado = $this->model_navegacion->nuevoSubMenu();
        redirect( base_url('administracion/menu/submenu/'. $resultado, 'location') );
    }

    public function getSubmenus()
    {
        if (!$this->input->is_ajax_request()) {
           exit('No direct script access allowed');
        }
        $id_menu_principal = $this->input->get('menu');
        $resultado = $this->model_navegacion->getListadoSubMenus( $id_menu_principal );
        echo $resultado;
    }

}

/* End of file menu.php */
/* Location: ./application/controllers/menu.php */