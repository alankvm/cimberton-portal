<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asignar extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_administracion');
        $this->load->model('model_navegacion');
        $this->load->model('model_alertas');
    }

    public function index()
    {
        
    }

    public function permisos()
    {
        $labels = array(
            array( 'titulo' => 'Administraci&oacute;n - Asignaci&oacute;n de Permisos' ),
            array( 'ubicacion' => 'Accesibilidad' )
            );
        $js = array(
            'js/cimberton/administracion/accesibilidad/permisos.js'
            );
        
        $elementos = array(
            'form',
            'widget'
            );

        $view = 'administracion/asignar/permisos.php';

        $data['listado_cargos'] = $this->model_administracion->getListadoCargos();
        $data['listado_menus'] = $this->model_administracion->getListadoMenusNavegacion();
        if ( !$this->autentificacion->isLoggedIn() ) { 
            redirect();
        } else {
            $this->masterpage->cargar( $view, $labels, $data, $elementos, $js );
        }
    }

    public function opciones()
    {
        $labels = array(
            array( 'titulo' => 'Administraci&oacute;n - Asignaci&oacute;n de Permisos' ),
            array( 'ubicacion' => 'Accesibilidad' )
            );
        $js = array(
            'js/cimberton/administracion/accesibilidad/permisos.js'
            );
        
        $elementos = array(
            'form',
            'widget'
            );

        $view = 'administracion/asignar/opciones.php';

        $id_cargo = $this->input->post('cargos');
        if ( !isset($id_cargo) || $id_cargo < 1 ) {
            redirect('administracion/asignar/permisos');
        }
        $data['listado_menus'] = $this->model_administracion->getListadoMenusNavegacion( $id_cargo );
        $data['id_cargo'] = $id_cargo;
        if ( !$this->autentificacion->isLoggedIn() ) { 
            redirect();
        } else {
            $this->masterpage->cargar( $view, $labels, $data, $elementos, $js );
        }
    }

    public function establecer()
    {
        //Asignar permisos de navegacion        
        $resultado = $this->model_administracion->setPermisosNavegacionCargo( $this->input->post() );
        redirect( 'administracion/asignar/permisos/'. $resultado, 'location' );
    }

}

/* End of file asignar.php */
/* Location: ./application/controllers/asignar.php */