<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Control extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_gerencia');
    }

    public function index()
    {

        $labels = array(
            array( 'titulo' => 'Panel de Control - Inicio' ),
            array( 'ubicacion' => 'Panel de Control' )
            );
        
        $elementos = array(
            'form',
            'widget'
            );
        $js = array(
            'js/cimberton/tablero/gerencia/resumen.js'
        );

        $data['filtro'] = $this->model_gerencia->filtroPeriodos();
        $view = 'panel/control.php';
        
        if ( !$this->autentificacion->isLoggedIn() ) { 
            redirect();
        } else {
            $this->masterpage->cargar( $view, $labels, $data, $elementos, $js );
        }
        
    }

}

/* End of file control.php */
/* Location: ./application/controllers/control.php */