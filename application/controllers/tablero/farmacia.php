<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Farmacia extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_farmacia');
        $this->load->helper('totales_helper');
        $this->load->helper('modales_helper');
        $this->load->helper('clear_names_helper');
    }

    public function index()
    {
        redirect('tablero/farmacia/resumen');
    }

    public function resumen()
    {
        $labels = array(
            array( 'titulo' => 'Farmacia - Resumen' ),
            array( 'ubicacion' => 'Resumen' )
            );
        $js = array(
            'js/cimberton/tablero/farmacia/resumen.js'
            );
        
        $elementos = array(
            'form',
            'widget',
            'table'
            );

        $view = 'tablero/farmacia/resumen.php';

        $data['periodo']        = $this->model_farmacia->getStringPeriodo();
        $dias                   = $this->model_farmacia->getDatosResumenCanalRegion();
        $data['dias_objetivos'] = $dias->DIAS_MES;
        $data['dias_efectivos'] = $dias->DIAS_TRANSCURRIDOS;

        $totales                = $this->model_farmacia->getObjetivoVentasCobros()->row();
        $data['pronostico']     = $totales->PRONOSTICO;
        $data['cobro']          = $totales->COBRO; 
        
        $data['distribucion']   = $this->model_farmacia->getVentasCanalDistribucion();
        $data['total_distri']   = $this->model_farmacia->getTotales( $data['distribucion'] );
        $data['regional']       = $this->model_farmacia->getVentasCanalPrivado();
        $data['total_region']   = $this->model_farmacia->getTotales( $data['regional'] );


        if ( !$this->autentificacion->isLoggedIn() ) { 
            redirect();
        } else {
            $this->masterpage->cargar( $view, $labels, $data, $elementos, $js );
        }
    }
    
    public function privado()
    {
        $labels = array(
            array( 'titulo' => 'Farmacia - Ventas Canal Privado' ),
            array( 'ubicacion' => 'Canal Privado' )
            );
        $js = array(
            'js/cimberton/tablero/farmacia/privado.js'
            );
        
        $elementos = array(
            'form',
            'widget',
            'table'
            );

        $view = 'tablero/farmacia/privado.php';

        $data['periodo']  = $this->model_farmacia->getStringPeriodo();
        $data['listado']  = $this->model_farmacia->getResumenCanalPrivado();
        $data['total']    = $this->model_farmacia->getTotalesCondicionales( $data['listado'], 2000, "mayor" );
        $data['otros']    = $this->model_farmacia->getTotalesCondicionales( $data['listado'], 2000, "menor" );
        $data['pyg']      = $this->model_farmacia->getMarcasPyG();
        $data['totalpyg'] = $this->model_farmacia->getTotales( $data['pyg'] );

        if ( !$this->autentificacion->isLoggedIn() ) { 
            redirect();
        } else {
            $this->masterpage->cargar( $view, $labels, $data, $elementos, $js );
        }
    }

    public function licitacion()
    {
        $labels = array(
            array( 'titulo' => 'Farmacia - Licitaciones' ),
            array( 'ubicacion' => 'Licitaciones' )
            );
        $js = array(
            'js/cimberton/tablero/farmacia/privado.js'
            );
        
        $elementos = array(
            'form',
            'widget',
            'table'
            );

        $view = 'tablero/farmacia/licitacion.php';

        $data['periodo'] = $this->model_farmacia->getStringPeriodo();
        $data['listado'] = $this->model_farmacia->getResumenLicitaciones();
        $data['total']   = $this->model_farmacia->getTotales( $data['listado'] );

        if ( !$this->autentificacion->isLoggedIn() ) { 
            redirect();
        } else {
            $this->masterpage->cargar( $view, $labels, $data, $elementos, $js );
        }
    }

    public function vendedores()
    {
        $labels = array(
            array( 'titulo' => 'Farmacia - Vendedores' ),
            array( 'ubicacion' => 'Vendedores' )
            );
        $js = array(
            'js/cimberton/tablero/farmacia/privado.js',
            'js/cimberton/tablero/farmacia/resumen.js'
            );
        
        $elementos = array(
            'form',
            'widget',
            'table'
            );

        $view = 'tablero/farmacia/vendedores.php';

        $data['periodo'] = $this->model_farmacia->getStringPeriodo();
        $data['listado'] = $this->model_farmacia->getResumenVendedores();
        $data['total']   = $this->model_farmacia->getTotales( $data['listado'] );

        if ( !$this->autentificacion->isLoggedIn() ) { 
            redirect();
        } else {
            $this->masterpage->cargar( $view, $labels, $data, $elementos, $js );
        }
    }

}

/* End of file farmacia.php */
/* Location: ./application/controllers/farmacia.php */