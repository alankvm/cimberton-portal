<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gerencia extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_gerencia');
    }

    public function index()
    {
        redirect('tablero/gerencia/resumen');
    }

    /**
     * Cargar Tablero de Resumen
     */
    public function resumen()
    {
        $labels = array(
            array( 'titulo' => 'Gerencia - Resumen' ),
            array( 'ubicacion' => 'Resumen' )
            );
        $js = array(
            'js/cimberton/tablero/gerencia/resumen.js'
            );
        
        $elementos = array(
            'form',
            'widget',
            'table'
            );

        $view = 'tablero/gerencia/resumen.php';

        $data['resumen_gerencia'] = $this->model_gerencia->getTableroResumenGerencia();
        
        $data['periodo'] = $this->model_gerencia->getStringPeriodo();
        $data['filtro'] = $this->model_gerencia->filtroPeriodos();

        if ( !$this->autentificacion->isLoggedIn() ) { 
            redirect();
        } else {
            $this->masterpage->cargar( $view, $labels, $data, $elementos, $js );
        }
    }

    /**
     * Aplicar filtrado de informacion
     * @return GET retorna a la pagina de origen
     */
    public function filtrar()
    {
        $mes          = $this->input->post('mes');
        $es_acumulado = $this->input->post('acumulado');
        $year         = $this->input->post('year');
        
        $this->model_gerencia->setPeriodoActivo( $mes, $year, $es_acumulado );    

        redirect( $_SERVER['HTTP_REFERER'] );
    }

    

}

/* End of file gerencia.php */
/* Location: ./application/controllers/gerencia.php */