jQuery(document).ready(function($) {
    //------------- Navigation -------------//

    mainNav = $('.mainnav>ul>li');
    mainNav.find('ul').siblings().addClass('hasUl').append('<span class="hasDrop icon16 icomoon-icon-arrow-down-2"></span>');
    mainNavLink = mainNav.find('a').not('.sub a');
    mainNavLinkAll = mainNav.find('a');
    mainNavSubLink = mainNav.find('.sub a').not('.sub li .sub a');
    mainNavCurrent = mainNav.find('a.current');

    //remove current class if have
    mainNavCurrent.removeClass('current');
    //set the seleceted menu element
    if ($.cookie("newCurrentMenu")) {
        mainNavLinkAll.each(function(index) {
            if($(this).attr('href') == $.cookie("newCurrentMenu")) {
                //set new current class
                $(this).addClass('current');

                ulElem = $(this).closest('ul');
                if(ulElem.hasClass('sub')) {
                    //its a part of sub menu need to expand this menu
                    aElem = ulElem.prev('a.hasUl').addClass('drop');
                    ulElem.addClass('expand');
                } 
                //create new cookie
                $.cookie("currentPage",$(this).attr('href'));
            }
        });
    }   
    
    //hover magic add blue color to icons when hover - remove or change the class if not you like.
    mainNavLinkAll.hover(
      function () {
        $(this).find('span.icon16').addClass('blue');
      }, 
      function () {
        $(this).find('span.icon16').removeClass('blue');
      }
    );

    //click magic
    mainNavLink.click(function(event) {
        $this = $(this);
        
        if($this.hasClass('hasUl')) {
            event.preventDefault();
            if($this.hasClass('drop')) {
                $(this).siblings('ul.sub').slideUp(500, 'jswing').siblings().removeClass('drop');
            } else {
                $(this).siblings('ul.sub').slideDown(500, 'jswing').siblings().addClass('drop');
            }           
        } else {
            //has no ul so store a cookie for change class.
            $.cookie("newCurrentMenu",$this.attr('href') ,{expires: null});
        }
    });
    mainNavSubLink.click(function(event) {
        $this = $(this);
        
        if($this.hasClass('hasUl')) {
            event.preventDefault();
            if($this.hasClass('drop')) {
                $(this).siblings('ul.sub').slideUp(500).siblings().removeClass('drop');
            } else {
                $(this).siblings('ul.sub').slideDown(250).siblings().addClass('drop');
            }           
        } else {
            //has no ul so store a cookie for change class.
            $.cookie("newCurrentMenu",$this.attr('href') ,{expires: null});
        }
    });

    //responsive buttons
    $('.resBtn>a').click(function(event) {
        $this = $(this);
        if($this.hasClass('drop')) {
            $('#sidebar>.shortcuts').slideUp(500).addClass('hided');
            $('#sidebar>.sidenav').slideUp(500).addClass('hided');
            $('#sidebar-right>.shortcuts').slideUp(500).addClass('hided');
            $('#sidebar-right>.sidenav').slideUp(500).addClass('hided');
            $this.removeClass('drop');
        } else {
            if($('#sidebar').length) {
                $('#sidebar').css('display', 'block');
                if($('#sidebar-right').length) {
                    $('#sidebar-right').css({'display' : 'block', 'margin-top' : '0'});
                }
            }
            if($('#sidebar-right').length) {
                $('#sidebar-right').css('display', 'block');
            }
            $('#sidebar>.shortcuts').slideDown(250);
            $('#sidebar>.sidenav').slideDown(250);
            $('#sidebar-right>.shortcuts').slideDown(250);
            $('#sidebar-right>.sidenav').slideDown(250);
            $this.addClass('drop');
        }
    });
    $('.resBtnSearch>a').click(function(event) {
        $this = $(this);
        if($this.hasClass('drop')) {
            $('.search').slideUp(500);
            $this.removeClass('drop');
        } else {
            $('.search').slideDown(250);
            $this.addClass('drop');
        }
    });
});