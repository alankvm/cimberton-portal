var perfil = {
    validar: function () {
        $('#frm_perfil').validate({
            rules: {
                old_password: {
                    required: true
                },
                new_password: {
                    required: true,
                },
                confirm_new_password: {
                    required: true,
                    equalTo: "#new_password"
                }
            },
            messages: {
                old_password: {
                    required: "Por razones de seguridad, debes digitar t&uacute; contrase&ntilde;a actual para actualizar t&uacute; perfil"
                },
                new_password: {
                    required: "Este campo es obligatorio"
                },
                confirm_new_password: {
                    required: "Este campo es obligatorio",
                    equalTo: "La contrase&ntilde;a digitada no concuerda por favor, intentalo de nuevo"
                }
            }
        });
    },
    onReady: function () {
        perfil.validar();
    },
    init: function () {
        perfil.onReady();
    }
};

jQuery(document).ready(function($) {
    perfil.init();
});