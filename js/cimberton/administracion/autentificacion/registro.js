var registro = {
    fn: {
        generarCodigoEmpleado: function () {
            var nombres = $("#field-USU_NOMBRE"),
                apellidos = $("#field-USU_APELLIDO"),
                $usuario = $("#field-USU_USUARIO"),
                codigo = '';
            codigo = nombres.val().substr(0, 1).toLowerCase() + apellidos.val().split(' ')[0].toLowerCase();
            return $usuario.val(codigo);
        }
    },
    ajax: {
        comprobarUsuarioSybase: function () {
            var url = gSateliteWhite.baseUrl("login/validarUsuarioSybase"),
                $usuario = $("#field-USU_USUARIO"),
                datos = {
                    usuario: $usuario.val()
                };

            $.ajax({
                url: url,
                dataType: "JSON",
                data: datos,
                type: "GET",
                success: function (data) {
                    if (data === false) {
                        $.pnotify({
                            type: 'error',
                            title: 'Oh No!',
                            text: 'Usuario No Registrado en SPC.',
                            icon: 'picon icon24 typ-icon-cancel white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                    } else  {
                        $.pnotify({
                            type: 'success',
                            title: 'Operaci&oacute;n Completada',
                            text: 'Usuario Registrado en SPC',
                            icon: 'picon icon16 iconic-icon-check-alt white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                    }
                }
            });
        }
    },
    onReady: function () {

        if ($("#field-USU_NOMBRE") && $("#field-USU_APELLIDO") && $("#field-USU_USUARIO")) {
            $("#field-USU_USUARIO").prop("readonly", true);
            $("#field-USU_NOMBRE").on("keyup", registro.fn.generarCodigoEmpleado);
            $("#field-USU_APELLIDO").on("keyup", registro.fn.generarCodigoEmpleado);
            $("select").on("change", registro.ajax.comprobarUsuarioSybase);
        }
    },
    init: function () {
        registro.onReady();
    }
};

jQuery(document).ready(function ($) {
    registro.init();
});