var nuevoMenu = {
    fn: {
        limpiarBloquearUrl: function () {
            var $inputURL = $("#url"),
                dataTemporal = $inputURL.val();
            //Verifica accion a tomar
            if ($(this).is(":checked")) {
                //Almacenar data arbitraria en input para memoria
                $inputURL.data('old-url', dataTemporal);
                //Cambiar estado del input
                $inputURL.val("#").prop("readonly", true);    
            } else {
                //Restarblecer valor en memoria y remover el bloqueo
                $inputURL.val($inputURL.data('old-url')).prop("readonly", false);    
            }
            
        },
        agregarIconoSeleccionado: function () {
            var iconoSeleccionado = $(this).data('icon'),
                $campoIconoSeleccionado = $("#selected_icon"),
                $hiddenIcono = $("#hidden_icono");

            $campoIconoSeleccionado.find("#display_icon").removeClass().addClass(iconoSeleccionado);
            $hiddenIcono.val(iconoSeleccionado);
        },
        agregarNombreOpcion: function () {
            var $campoIconoSeleccionado = $("#display_icon");
            $campoIconoSeleccionado.text(' ' + $(this).val());
        },
        llenarDropDownSubMenus: function (rows) {
            var $dropDownSubmenus = $("select[name='submenu']"),
                opciones = '<option></option>';
            $.each(rows, function (index, element) {
                opciones += '<option value="' + element.id + '">' + element.nombre + '</option>';
            }); 

            $dropDownSubmenus.select2('destroy');
            $dropDownSubmenus.empty().append(opciones);
            return $dropDownSubmenus.select2();
        }
    },
    validacion: function () {
        $("#nuevo_menu").validate({
            rules: {
                nombre: {
                    required: true,
                    minlength: 3
                },
                url: {
                    required: true
                }
            },
            messages: {
                nombre: {
                    required: "Este campo es obligatorio",
                    minlength: "Por favor, ingrese almenos 3 caracteres"
                },
                url: {
                    required: "Este campo es obligatorio"
                }
            }
        });
    },
    ajax: {
        getListadoSubMenus: function () {
            var url = gSateliteWhite.baseUrl('menu/getSubMenus'),
                datos = {
                    menu: $(this).find(":selected").val()
                };

            $.ajax({
                url: url,
                dataType: "JSON",
                data: datos,
                type: "GET",
                success: function (data) {
                    nuevoMenu.fn.llenarDropDownSubMenus(data);
                }
            });
        }
    }, 
    onReady: function () {
        if ($('textarea').hasClass('limit')) {
            $('.limit').inputlimiter({
                remText: '%n caracteres restantes.',
                limit: 80,
                limitText: 'Limite del campo %n caracteres.'
            });
        }
        if ($("input[type='checkbox']").hasClass("ibuttonCheck")) {
            $(".ibuttonCheck").iButton({
                 labelOn: "<span class='icon16 icomoon-icon-checkmark-2 white'></span>",
                 labelOff: "<span class='icon16 icomoon-icon-cancel-3 white'></span>",
                 enableDrag: false
            }).on("change", nuevoMenu.fn.limpiarBloquearUrl);
        }
        if ($("select").hasClass("nostyle")) {
            $("select.nostyle").select2();
        }
        if($(".scroll").length) {
            $(".scroll").niceScroll({
                cursoropacitymax: 0.7,
                cursorborderradius: 6,
                cursorwidth: "7px"
            });
        }
        
        $("span.iconos").on("dblclick", nuevoMenu.fn.agregarIconoSeleccionado);
        $("#opcion").on("keyup", nuevoMenu.fn.agregarNombreOpcion);
        $("select[name='principal']").on("change", nuevoMenu.ajax.getListadoSubMenus);
        
        nuevoMenu.validacion();
    }, 
    init: function () {
        nuevoMenu.onReady();
    }
};

jQuery(document).ready(function($) {
    nuevoMenu.init();
});