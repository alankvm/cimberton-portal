var permisos = {
    fn: {
        mostrarOcultarOpciones: function () {
            var $this = $(this),
                $tablaOpciones = $this.next().next();

            $tablaOpciones.toggle(300);
            setTimeout(permisos.fn.verificarEstadoOpcion, 300);
            if (!$this.is(":checked")) {
                permisos.fn.limpiarSeleccion( $tablaOpciones );
            }
        },
        limpiarSeleccion: function ( $tablaOpciones ) {

            $tablaOpciones.find("input[type='checkbox']").removeAttr("checked");
        },
        verificarEstadoOpcion: function () {
            var opciones = $("input[name='menu_opciones[]']");

            $.each(opciones, function(index, element) {
                 if($(element).data("check")) {
                    $(element).prop("checked", true);
                 }
            });
        },
        verificarEstadoMenu: function () {
            var menus = $("input[name='sub_menus[]']");
            $.each(menus, function(index, element) {
                 if (!$(element).is(":checked")) {
                    $(element).next().next().toggle();
                    permisos.fn.verificarEstadoOpcion();
                 }
            });
        }
    },
    onReady: function () {
        $("select").select2();
        permisos.fn.verificarEstadoMenu();
        $("input[name='sub_menus[]']").on("click", permisos.fn.mostrarOcultarOpciones)
    }, 
    init: function () {
        permisos.onReady();
        permisos.fn.verificarEstadoOpcion();
    }
};

jQuery(document).ready(function ($) {
    permisos.init();
});