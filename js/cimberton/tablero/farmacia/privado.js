var privado = {
    resaltarTitulo: function () {
        //Obtener todas las celdas de la columna Canal
        var titulos = $("table tbody").find("tr").find("td:first"),
            tituloFiltrados = null;
debugger;
        //Filtrar todas las celdas y buscar las celdas titulo
        tituloFiltrados = _.filter(titulos, function (titulo) {
            return $(titulo).text() === "GRAN TOTAL";
        });
        //Agregar estilo resaltado a cada celda
        $(tituloFiltrados).parent().addClass("strong").css("background-color", "#e3e3e3");

    },
    mostrarOtrasMarcas: function (event) {
        $("#marcas").modal({
            show: true
        });
    },
    mostrarOtrasPyG: function (event) {
        $("#pyg-marcas").modal({
            show: true
        });
    },
    onReady: function () {
        //Alinear Numeros a la derecha
        $("table tbody").find("td").not(":nth-child(1)").not(":nth-child(2)").css("text-align", "right");
        //Alinear texto la izquierda
        $("table tbody").find("td:nth-child(2)").css("text-align", "left");
        //Mostrar modal de otros productos 
        $("a#otras-marcas").on("click", privado.mostrarOtrasMarcas);
        $("a#pyg").on("click", privado.mostrarOtrasPyG);
    }
};

jQuery(document).ready(function ($) {
    privado.onReady();
    privado.resaltarTitulo();
});