/*
    Aplica estilo de separadores para las tablas con resumen de venta y cobro
 */
var separadorTabla = {
    //configuracion basica
    config: function (selector) {
        separadorTabla.applyStyle(selector);
    },
    //Aplicacion de estilo
    applyStyle: function (selector) {
        var tipoSector = {
            thead: "th",
            tbody: "tr"
        }
        //Por cada index de tipo de sector
        $.each(tipoSector, function (index, element) {
            //Construir selector principal
            var definer = "table " + index;
            //Aplicar estilo pre-definidos
            $(definer)
                .find("tr")
                .find(selector)
                .css({
                    borderRightWidth: "1px",
                    borderRightStyle: "double",
                    borderRightColor: "#C4C4C4"
                });
        });
    },
    resaltarTitulos: function () {
        //Obtener todas las celdas de la columna Canal
        var titulos = $("table tbody").find("tr").find("td:first"),
            tituloFiltrados = null;

        //Filtrar todas las celdas y buscar las celdas titulo
        tituloFiltrados = _.filter(titulos, function (titulo) {
            return $(titulo).text() === "GRAN TOTAL";
        });
        //Agregar estilo resaltado a cada celda
        $(tituloFiltrados).parent().addClass("strong").css("background-color", "#e3e3e3");

    },
    //Inicializar configuracion inicial
    init: function () {
        separadorTabla.resaltarTitulos();
        separadorTabla.config(":first");
        separadorTabla.config(":nth-child(6)");
    }
};

jQuery(document).ready(function ($) {
    separadorTabla.init();
    $("table tbody").find("td").css("text-align", "right");
});