/*
    Aplica estilo de separadores para las tablas con resumen de venta y cobro
 */
var separadorTabla = {
    //configuracion basica
    config: function (selector) {
        separadorTabla.applyStyle(selector);
    },
    //Aplicacion de estilo
    applyStyle: function (selector) {
        var tipoSector = {
            thead: "th",
            tbody: "tr"
        };
        //Por cada index de tipo de sector
        $.each(tipoSector, function (index, element) {
            //Construir selector principal
            var definer = "table " + index;
            //Aplicar estilo pre-definidos
            $(definer)
                .find("tr")
                .find(selector)
                .css({
                    borderRightWidth: "1px",
                    borderRightStyle: "double",
                    borderRightColor: "#C4C4C4"
                });
        });
    },
    //Inicializar configuracion inicial
    init: function () {
        separadorTabla.config(":first");
        separadorTabla.config(":nth-child(6)");
    }
};

jQuery(document).ready(function ($) {
    separadorTabla.init();
    $("table tbody").find("td").css("text-align", "right");
    (function () {
        function resaltarTitulo() {
            //Obtener todas las celdas de la columna Canal
            var titulos = $("table#ventas tbody").find("tr").find("td:first"),
                tituloFiltrados = null;

            //Filtrar todas las celdas y buscar las celdas titulo
            tituloFiltrados = _.filter(titulos, function (titulo) {
                return $(titulo).text() === "DIVISION FARMACIA" || $(titulo).text() === "DIVISION CONSUMO" || $(titulo).text() === "TOTAL EMPRESA";
            });
            //Agregar estilo resaltado a cada celda
            $(tituloFiltrados).parent().addClass("strong").css("background-color", "#e3e3e3");

        }

        function cargarConsolidado() {
            $(".ibutton1").iButton({
                labelOn: "Si",
                labelOff: "No",
                enableDrag: false
            });
        }

        function esAcumulado() {
            $("#acumulado").on('change', function () {
                var selectMonth = $("#mes");

                if ($(this).is(":checked")) {
                    selectMonth.select2("data", null);
                }
            });
        }

        function cargarSelects() {
            $("select").select2();
        }

        function verificarYear() {
            $("form#frm-filtro").on("submit", function () {
                var dropDownYear = $("select#year"),
                    dropDownMonth = $("select#mes"),
                    acumulado = $("#acumulado");

                if (dropDownYear.find(":selected").val() === "") {
                    $.pnotify({
                        type: 'error',
                        title: 'Advertencia',
                        text: 'El campo a&ntilde;o es obligatorio.',
                        icon: 'picon icon24 typ-icon-cancel white',
                        opacity: 0.95,
                        sticker: false,
                        history: false
                    });
                    return false;
                }

                if (!acumulado.is(":checked") && dropDownMonth.find(":selected").val() === "") {
                    $.pnotify({
                        type: "error",
                        title: 'Advertencia',
                        text: 'El campo mes es obligatorio.',
                        icon: 'picon icon24 typ-icon-cancel white',
                        opacity: 0.95,
                        sticker: false,
                        history: false
                    });
                    return false;
                }
            });
        }

        return {
            resaltarTitulo: resaltarTitulo(),
            cargarConsolidado: cargarConsolidado(),
            esAcumulado: esAcumulado(),
            cargarSelects: cargarSelects(),
            verificarYear: verificarYear()
        };
    })();
});