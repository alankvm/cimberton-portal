# Portal para *C Imberton S.A de C.V* 
## FASE 1
#### Inicio de Labores el Dia: 2013.10.22 [YYYY.MM.DD]
#### 2013.10.23
* Inicio de adaptacion de plantilla para el proyecto
#### 2013.10.24
* Finalizacion de adaptacion de plantilla para el proyecto
* Inicio de Modulo de administracion de menus
* Inicio de Libreria de adaptacion a pagina maestra
#### 2013.10.25
* Finalizacion de Modulo de administracion de menus
* Finalizacion de libreria adaptacion a pagina maestra
#### 2013.10.28
* Inicio Adaptacion de Grocery CRUD a la plantilla actual
* Finalizacion de Adaptacion de Grocery CRUD  _Corregido: Eliminar y Ver en las views del tema datatables del grocerycrud_
* Inicio Modulo de gestion de autentificacion para accesos.
* Inicio Validaciones en vista de registro de usuario y acceso a la base de Sybase y verificacion de usuario unico
* Finalizacion registro de usuario y acceso a Sybase
#### 2013.10.29
* Inicio libreria de acceso al sistema
* Finalizacion libreria de acceso al sistema 
* Inicio Modulo de asignacion de permisos
#### 2013.10.30
* Finalizacion Modulo de asignacion de permisos y restriccion de acceso a funcionalidades.
#### 2013.10.31
* Actualizacion modulo de autentificacion de acceso al sistema: verifica primer inicio de sesion
* Actualizacion de perfil desde la aplicacion
#### 2013.11.01
* Reunion con jefe de informatica C Imberton
#### 2013.11.04
* Correccion de observaciones reunion 2013.11.01
## FASE 2
#### Inicio de labores el Dia: 2013.11.05 
* Volcar informacion de prueba para inicio de labores
* Normalizar informacion 
* Generar logica de consulta de reportes y agilizacion de acceso a reportes.
#### 2013.11.06
* Generacion de pantallas de tablero resumen
#### 2013.11.08 
* Generacion de filtrado de periodo para tablero resumen
* Observaciones de filtrado 
#### 2013.11.11
* Inicio de reporte resumen farmarcia
* Reingenieria de adaptacion para patrones de desarrollo
* clases de configuracion coupling
#### 2013.11.12
* Finalizacion reporte resumen farmacia 
    ![Resumen Farmacia](http://nov.imghost.us/gLwp.png "Resumen Farmacia") 
* Remover el inline-css de la vista y pasarlo a JavaScript